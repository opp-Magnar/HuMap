package hr.fer.opp.projekt.util.holders


import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.view.isVisible
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.request.RequestOptions
import com.google.android.material.card.MaterialCardView
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.firebase.storage.FirebaseStorage
import hr.fer.opp.projekt.R
import hr.fer.opp.projekt.model.LoginViewModel
import hr.fer.opp.projekt.model.Role
import hr.fer.opp.projekt.model.User
import hr.fer.opp.projekt.util.GlideApp

class UserHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    private val cardUsers: MaterialCardView = itemView.findViewById(R.id.cardUsers)
    private val collapsedLayout: ConstraintLayout = itemView.findViewById(R.id.collapsedUser)
    private val expandedLayout: ConstraintLayout = itemView.findViewById(R.id.expandedUser)
    private val ivCollapsed: ImageView = itemView.findViewById(R.id.ivCollapsedUser)
    private val ivExpanded: ImageView = itemView.findViewById(R.id.ivExpandedUser)
    private val tvCollapsedName: TextView = itemView.findViewById(R.id.tvCollapsedName)
    private val tvExpandedName: TextView = itemView.findViewById(R.id.tvExpandedName)
    private val tvCollapsedRole: TextView = itemView.findViewById(R.id.tvCollapsedRole)
    private val tvExpandedRole: TextView = itemView.findViewById(R.id.tvExpandedRole)
    private val tvExpandedPhone: TextView = itemView.findViewById(R.id.tvPhoneExpanded)
    private val btnActivateUser: Button = itemView.findViewById(R.id.btnActivateUser)
    private val btnChangeRole: Button = itemView.findViewById(R.id.btnChangeRole)

    fun setUser(user: User, context: Context?, loginViewModel: LoginViewModel, fragmentManager: FragmentManager) {
        val photoRef = FirebaseStorage.getInstance().getReferenceFromUrl(user.photoUrl)
        GlideApp.with(context!!)
                .load(photoRef)
                .apply(RequestOptions.circleCropTransform())
                .into(ivCollapsed)
        GlideApp.with(context)
                .load(photoRef)
                .into(ivExpanded)

        tvCollapsedName.text = user.displayName
        tvExpandedName.text = user.displayName

        tvCollapsedRole.text = user.role.toString()
        tvExpandedRole.text = user.role.toString()
        tvExpandedPhone.text = user.phoneNumber

        btnActivateUser.isVisible = !user.active
        btnActivateUser.setOnClickListener { loginViewModel.activateUser(user = user) }

        btnChangeRole.setOnClickListener {
            val roles = arrayListOf("ADMIN", "VODITELJ", "KARTOGRAF", "SPASIOC")
            roles.remove(user.role.toString())
            UserRoleDialogFragment(roles, user, loginViewModel).show(fragmentManager, "Dialog")
        }

        cardUsers.setOnClickListener {
            collapsedLayout.isVisible = !collapsedLayout.isVisible
            expandedLayout.isVisible = !expandedLayout.isVisible
        }
    }

}

class UserRoleDialogFragment(private val roles: ArrayList<String>, private val user: User, private val loginViewModel: LoginViewModel) : DialogFragment() {

    override fun onCreateDialog(
            savedInstanceState: Bundle?
    ): Dialog = MaterialAlertDialogBuilder(requireContext())
            .setTitle("Change user role")
            .setItems(roles.toTypedArray()) { _, which ->
                val role = when (roles[which]) {
                    "VODITELJ" -> Role.VODITELJ
                    "KARTOGRAF" -> Role.KARTOGRAF
                    "SPASIOC" -> Role.SPASIOC
                    else -> Role.ADMIN
                }
                loginViewModel.changeUserRole(user = user, role = role)
            }
            .create()

}