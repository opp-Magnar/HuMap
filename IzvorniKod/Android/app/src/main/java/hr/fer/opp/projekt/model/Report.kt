package hr.fer.opp.projekt.model

import com.google.firebase.firestore.DocumentId
import com.google.firebase.firestore.Exclude
import java.io.Serializable
import java.util.*

data class Report(
        @DocumentId val documentId: String = "",
        var userUid: String? = null,
        val firstName: String = "",
        val lastName: String = "",
        var photoUrl: String? = null,
        val description: String = "",
        val creationDate: Date = Date(),
        var locked: Boolean = false,
        var lockedDate: Date? = null,
        val comments: ArrayList<Comment> = arrayListOf()
) : Serializable {
    @get:Exclude
    val displayName: String by lazy { "$firstName $lastName" }
}
