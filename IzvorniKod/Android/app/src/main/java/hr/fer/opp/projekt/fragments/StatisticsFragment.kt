package hr.fer.opp.projekt.fragments


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import hr.fer.opp.projekt.R
import hr.fer.opp.projekt.controllers.FragmentController
import hr.fer.opp.projekt.model.LoginViewModel
import hr.fer.opp.projekt.model.Role
import hr.fer.opp.projekt.util.adapters.PageAdapter
import kotlinx.android.synthetic.main.fragment_statistics.*

class StatisticsFragment : FragmentController() {

    private val adapter by lazy { PageAdapter(childFragmentManager, 3) }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? =
            inflater.inflate(R.layout.fragment_statistics, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewPager.adapter = adapter
        statisticsTabs.setupWithViewPager(viewPager)
    }

    override fun onChanged(authState: LoginViewModel.AuthenticationState?) =
            if (authState == LoginViewModel.AuthenticationState.AUTHENTICATED && loginViewModel.currentUser!!.role != Role.ADMIN &&
                    loginViewModel.currentUser!!.role != Role.KARTOGRAF && loginViewModel.currentUser!!.role != Role.VODITELJ) {
                mListener.navigateBottomAppBarReset(R.id.reports_nav)
            } else {
                super.onChanged(authState)
            }

}