package hr.fer.opp.projekt.util.holders

import android.content.Context
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.request.RequestOptions
import hr.fer.opp.projekt.R
import hr.fer.opp.projekt.model.Comment
import hr.fer.opp.projekt.model.LoginViewModel
import hr.fer.opp.projekt.model.User
import hr.fer.opp.projekt.util.GlideApp
import hr.fer.opp.projekt.util.listeners.AuthListener
import java.text.SimpleDateFormat
import java.util.*

class CommentHolder(itemView: View, private val context: Context, private val loginViewModel: LoginViewModel)
    : RecyclerView.ViewHolder(itemView), AuthListener {

    private val ivUser: ImageView = itemView.findViewById(R.id.ivUserComment)
    private val tvName: TextView = itemView.findViewById(R.id.tvUserCommentName)
    private val tvDesc: TextView = itemView.findViewById(R.id.tvUserCommentDesc)
    private val tvDate: TextView = itemView.findViewById(R.id.tvUserCommentDate)

    fun setComment(comment: Comment) {
        comment.userUid?.let {
            loginViewModel.getUserObjectAsync(it, this)
        }
        tvDesc.text = comment.comment
        tvDate.text = SimpleDateFormat("dd.MM.yyyy.", Locale.UK).format(comment.date)
    }

    override fun onBeginAuth() = Unit

    override fun onAuthenticated(user: User) {
        GlideApp.with(context)
                .load(user.photoUrl)
                .apply(RequestOptions.circleCropTransform())
                .into(ivUser)
        tvName.text = user.displayName
    }

    override fun onNotActivated(user: User) = onAuthenticated(user)

    override fun onFailureAuth(message: String) = Unit

}