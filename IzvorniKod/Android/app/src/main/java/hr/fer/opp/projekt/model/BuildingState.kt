package hr.fer.opp.projekt.model

enum class BuildingState {
    SEARCHED, UNSEARCHED
}