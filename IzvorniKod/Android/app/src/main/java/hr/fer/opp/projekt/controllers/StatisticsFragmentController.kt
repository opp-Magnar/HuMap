package hr.fer.opp.projekt.controllers


import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.github.mikephil.charting.charts.LineChart
import com.github.mikephil.charting.components.AxisBase
import com.github.mikephil.charting.components.Legend
import com.github.mikephil.charting.components.MarkerView
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import com.github.mikephil.charting.formatter.ValueFormatter
import com.github.mikephil.charting.highlight.Highlight
import com.github.mikephil.charting.utils.MPPointF
import com.google.firebase.firestore.FirebaseFirestore
import hr.fer.opp.projekt.R
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit

fun getFormattedDate(time: Long, mDataFormat: SimpleDateFormat): String = try {
    val mDate = Date().apply { this.time = time }
    mDataFormat.format(mDate)
} catch (ex: Exception) {
    "xx"
}

open class StatisticsFragmentController : Fragment() {

    // 1.1.2016.
    protected val timeReference = 1451602800000L

    private val today by lazy { days(Date()) }

    protected val db by lazy { FirebaseFirestore.getInstance() }

    private lateinit var chart: LineChart

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        chart = view.findViewById(R.id.chart)
        chartSetup()
    }

    private fun chartSetup() {
        chart.setExtraOffsets(10.toFloat(), 10.toFloat(), 25.toFloat(), 0.toFloat())

        chart.axisLeft.axisMinimum = 0.toFloat()
        chart.axisLeft.granularity = 1.0f
        chart.axisLeft.isGranularityEnabled = true

        chart.axisRight.setDrawLabels(false)
        chart.axisRight.axisMinimum = 0.toFloat()
        chart.axisRight.granularity = 1.0f
        chart.axisRight.isGranularityEnabled = true

        chart.xAxis.valueFormatter = DayAxisValueFormatter(timeReference)
        chart.xAxis.labelCount = 4
        chart.xAxis.position = XAxis.XAxisPosition.BOTTOM
        chart.xAxis.granularity = 1.0f
        chart.xAxis.isGranularityEnabled = true

        chart.legend.verticalAlignment = Legend.LegendVerticalAlignment.TOP
        chart.legend.horizontalAlignment = Legend.LegendHorizontalAlignment.LEFT
        chart.legend.orientation = Legend.LegendOrientation.HORIZONTAL
        chart.legend.setDrawInside(false)

        chart.description.isEnabled = false

        chart.animateY(500)
    }

    protected fun markerSetup(vararg messages: String) {
        if (messages.size != chart.lineData.dataSetCount) throw IllegalStateException()
        chart.marker = MyMarkerView(context, R.layout.marker_view, timeReference, *messages)
    }

    protected fun setChartData(vararg dataSets: LineDataSet) {
        val lineData = LineData(dataSets.asList())
        chart.data = lineData
        chart.invalidate()
    }

    private fun notifyChartDataChanged() {
        chart.lineData.notifyDataChanged()
        chart.notifyDataSetChanged()
        setVisibleRange()
    }

    private fun setVisibleRange() {
        chart.setVisibleXRangeMaximum(5.toFloat())
        chart.moveViewToX(today.toFloat())
        chart.setVisibleXRangeMaximum(100.toFloat())
    }

    private fun days(date: Date): Long = TimeUnit.MILLISECONDS.toDays((date.time - timeReference))

    protected fun fillChart(dates: List<Date>, dataset: LineDataSet, startDate: Date? = null) {
        // Align x axis of multiple graphs
        if (startDate != null && dates.isNotEmpty()) {
            val upperBound = days(dates[0])
            var current = days(startDate)
            while (current < upperBound) dataset.addEntry(Entry((current++).toFloat(), 0.toFloat()))
        }
        var count = 0
        var oldValue = 0L
        var newValue: Long? = null
        for (date in dates) {
            newValue = days(date)
            if (oldValue == 0L) oldValue = newValue
            if (newValue > oldValue) {
                dataset.addEntry(Entry(oldValue.toFloat(), count.toFloat()))
                oldValue++
                while (oldValue < newValue) {
                    dataset.addEntry(Entry(oldValue.toFloat(), 0.toFloat()))
                    oldValue++
                }
                count = 1
                oldValue = newValue
            } else {
                count++
            }
        }
        newValue?.let {
            dataset.addEntry(Entry(it.toFloat(), count.toFloat()))
            var v = it
            while (v++ < today) {
                dataset.addEntry(Entry(v.toFloat(), 0.toFloat()))
            }
        }

        dataset.notifyDataSetChanged()
        notifyChartDataChanged()
    }

    protected class DayAxisValueFormatter(private val timeReference: Long) : ValueFormatter() {

        private val mDataFormat = SimpleDateFormat("dd/MM/yy", Locale.UK)

        override fun getPointLabel(entry: Entry?): String = ""

        override fun getAxisLabel(value: Float, axis: AxisBase?): String {
            val convertedTime = TimeUnit.DAYS.toMillis(value.toLong())
            val originalTime = timeReference + convertedTime
            return getFormattedDate(originalTime, mDataFormat)
        }

    }

    @SuppressLint("ViewConstructor")
    protected class MyMarkerView(context: Context?, layoutResource: Int, private val referenceTimestamp: Long,
                                 private vararg val messages: String) : MarkerView(context, layoutResource) {

        private val tvContent: TextView = findViewById<View>(R.id.tvContent) as TextView
        private val mDataFormat = SimpleDateFormat("dd.MM.yyyy.", Locale.UK)

        override fun refreshContent(e: Entry, highlight: Highlight) {
            val currentTimestamp = TimeUnit.DAYS.toMillis(e.x.toLong()) + referenceTimestamp
            val text = e.y.toInt().toString() + " " + messages[highlight.dataSetIndex] + " at\n" +
                    getFormattedDate(currentTimestamp, mDataFormat)
            tvContent.text = text
            super.refreshContent(e, highlight)
        }

        override fun getOffset(): MPPointF? {
            return MPPointF((-(width / 2)).toFloat(), (-height).toFloat())
        }

    }

}