package hr.fer.opp.projekt.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.firebase.ui.firestore.FirestoreRecyclerOptions
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import hr.fer.opp.projekt.R
import hr.fer.opp.projekt.controllers.FragmentController
import hr.fer.opp.projekt.model.User
import hr.fer.opp.projekt.util.adapters.FirestoreAdapterUser
import kotlinx.android.synthetic.main.fragment_users.*

class UsersFragment : FragmentController() {

    private val adapter: FirestoreAdapterUser by lazy { setupAdapter() }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? =
            inflater.inflate(R.layout.fragment_users, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupRecyclerView()
    }

    private fun setupRecyclerView() {
        rvUsers!!.setHasFixedSize(true)
        rvUsers!!.layoutManager = LinearLayoutManager(context)
        rvUsers!!.adapter = adapter
    }

    private fun setupAdapter(): FirestoreAdapterUser {
        val query = FirebaseFirestore.getInstance()
                .collection("users")
                .orderBy("firstName")
                .orderBy("lastName")
                .limit(50)
        val options = FirestoreRecyclerOptions.Builder<User>()
                .setQuery(query, User::class.java)
                .build()
        return FirestoreAdapterUser(options, context!!, loginViewModel, activity!!.supportFragmentManager).apply {
            this.startListening()
        }
    }

}