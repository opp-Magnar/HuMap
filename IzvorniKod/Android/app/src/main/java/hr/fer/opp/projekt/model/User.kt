package hr.fer.opp.projekt.model

import com.google.firebase.firestore.DocumentId
import com.google.firebase.firestore.Exclude
import java.io.Serializable

data class User(
        @DocumentId val uid: String = "",
        var username: String = "",
        var firstName: String = "",
        var lastName: String = "",
        var role: Role = Role.SPASIOC,
        val email: String = "",
        var phoneNumber: String = "",
        var photoUrl: String = "",
        var active: Boolean = false
) : Serializable {
    @get:Exclude
    val displayName: String by lazy { "$firstName $lastName" }
}
