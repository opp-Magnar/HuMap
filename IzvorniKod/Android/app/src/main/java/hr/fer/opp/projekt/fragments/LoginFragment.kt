package hr.fer.opp.projekt.fragments


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import hr.fer.opp.projekt.R
import hr.fer.opp.projekt.controllers.FragmentController
import hr.fer.opp.projekt.model.LoginViewModel.AuthenticationState
import hr.fer.opp.projekt.model.User
import hr.fer.opp.projekt.util.listeners.AuthListener
import kotlinx.android.synthetic.main.fragment_login.*

class LoginFragment : FragmentController(), AuthListener {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? =
            inflater.inflate(R.layout.fragment_login, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        btnSignIn.setOnClickListener {
            clearErrors()
            val email = etLoginEmail!!.text.toString()
            val password = etLoginPassword!!.text.toString()
            if (email.isEmpty() || password.isEmpty()) {
                if (email.isEmpty()) {
                    tiLoginEmail!!.error = "This field is required"
                }
                if (password.isEmpty()) {
                    tiLoginPassword!!.error = "This field is required"
                }
                return@setOnClickListener
            }
            loginViewModel.authenticate(this, email, password)
        }
        btnRegister.setOnClickListener {
            navigateTo(R.id.action_login_to_register)
        }
    }

    override fun onResume() {
        super.onResume()
        clearErrors()
    }

    override fun onChanged(authState: AuthenticationState?) {
        if (authState === AuthenticationState.AUTHENTICATED) {
            mListener.navigateBottomAppBarReset(R.id.map_nav)
        } else {
            userChanged()
        }
    }

    override fun onBeginAuth() = mListener.begin()

    override fun onAuthenticated(user: User) = mListener.stop()

    override fun onNotActivated(user: User) {
        mListener.stop()
        mListener.showSnackbar("User is not activated!")
    }

    override fun onFailureAuth(message: String) {
        mListener.stop()
        mListener.showSnackbar(message)
    }

    private fun clearErrors() {
        tiLoginEmail!!.error = null
        tiLoginPassword!!.error = null
    }

}