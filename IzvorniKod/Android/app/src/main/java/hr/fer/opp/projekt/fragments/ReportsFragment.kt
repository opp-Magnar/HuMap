package hr.fer.opp.projekt.fragments


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.firebase.ui.firestore.FirestoreRecyclerOptions
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import hr.fer.opp.projekt.R
import hr.fer.opp.projekt.controllers.FragmentController
import hr.fer.opp.projekt.model.LoginViewModel
import hr.fer.opp.projekt.model.Report
import hr.fer.opp.projekt.model.Role
import hr.fer.opp.projekt.util.adapters.FirestoreAdapterReport
import hr.fer.opp.projekt.util.holders.ReportHolder
import hr.fer.opp.projekt.util.listeners.RecyclerViewOnClickListener
import kotlinx.android.synthetic.main.fragment_reports.*


class ReportsFragment : FragmentController(), RecyclerViewOnClickListener {

    private val adapter by lazy { setupAdapter() }

    private val db by lazy { FirebaseFirestore.getInstance() }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? =
            inflater.inflate(R.layout.fragment_reports, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupRecyclerView()
        fabAddReport.setOnClickListener {
            navigateTo(R.id.action_reports_to_addReportFragment)
        }
    }

    override fun onChanged(authState: LoginViewModel.AuthenticationState?) {
        userChanged()
    }

    private fun setupRecyclerView() {
        rvReports!!.setHasFixedSize(true)
        rvReports!!.layoutManager = LinearLayoutManager(context)
        rvReports!!.adapter = adapter
    }

    private fun setupAdapter(): FirestoreAdapterReport {
        val query = FirebaseFirestore.getInstance()
                .collection("reports")
                .orderBy("creationDate", Query.Direction.DESCENDING)
                .limit(50)
        val options = FirestoreRecyclerOptions.Builder<Report>()
                .setQuery(query, Report::class.java)
                .build()
        return FirestoreAdapterReport(options, context!!, this, loginViewModel).apply {
            this.startListening()
        }
    }

    override fun onClick(holder: ReportHolder, model: Report, position: Int, delete: Boolean) {
        if (delete) {
            loginViewModel.currentUser?.let {
                if (it.role != Role.ADMIN && it.role != Role.VODITELJ) return@let
                MaterialAlertDialogBuilder(context).setTitle("Delete report?").setPositiveButton("DELETE") { dialog, _ ->
                    db.collection("reports").document(model.documentId).delete()
                            .addOnFailureListener { exception: Exception -> onFailure(exception.localizedMessage!!) }
                    dialog.dismiss()
                }.setNegativeButton("CANCEL") { dialog, _ ->
                    dialog.dismiss()
                }.show()
            }
        } else {
            val bundle = Bundle()
            bundle.putSerializable("report", model)
            navigateTo(R.id.action_reports_to_reportDetails, bundle)
        }
    }

}
