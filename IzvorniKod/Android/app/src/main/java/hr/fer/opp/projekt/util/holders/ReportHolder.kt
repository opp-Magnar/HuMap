package hr.fer.opp.projekt.util.holders


import android.content.Context
import android.view.View
import android.widget.Button
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.TextView
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.request.RequestOptions
import com.google.android.material.card.MaterialCardView
import hr.fer.opp.projekt.R
import hr.fer.opp.projekt.model.LoginViewModel
import hr.fer.opp.projekt.model.Report
import hr.fer.opp.projekt.model.User
import hr.fer.opp.projekt.util.GlideApp
import hr.fer.opp.projekt.util.listeners.AuthListener
import hr.fer.opp.projekt.util.listeners.RecyclerViewOnClickListener
import java.text.SimpleDateFormat
import java.util.*

class ReportHolder internal constructor(itemView: View, private val listener: RecyclerViewOnClickListener,
                                        private val context: Context, private val loginViewModel: LoginViewModel) :
        RecyclerView.ViewHolder(itemView), AuthListener {

    private val cardHolder: MaterialCardView = itemView.findViewById(R.id.cardHolderReport)
    private val ivAvatar: ImageView = itemView.findViewById(R.id.ivAvatarReport)
    private val tvUsername: TextView = itemView.findViewById(R.id.tvUsernameReport)
    private val tvDate: TextView = itemView.findViewById(R.id.tvDateReport)
    private val ivPhoto: ImageView = itemView.findViewById(R.id.ivPhotoReport)
    private val btnComment: Button = itemView.findViewById(R.id.btnCommentReport)
    private val lockedView: FrameLayout = itemView.findViewById(R.id.lockedView)

    fun setReport(report: Report, position: Int) {
        ivPhoto.setImageResource(R.drawable.photo)
        ivAvatar.setImageResource(R.drawable.person)
        report.photoUrl?.let {
            GlideApp.with(context)
                    .load(it)
                    .into(ivPhoto)
        }
        report.userUid?.let {
            loginViewModel.getUserObjectAsync(it, this)
        }
        btnComment.text = report.comments.size.toString()
        tvUsername.text = report.displayName
        tvDate.text = SimpleDateFormat("dd.MM.yyyy.", Locale.UK).format(report.creationDate)
        if (report.locked) {
            lockedView.isVisible = true
            lockedView.setOnClickListener { listener.onClick(this, report, position, true) }
        } else {
            lockedView.isVisible = false
            cardHolder.setOnClickListener { listener.onClick(this, report, position, false) }
        }
    }

    override fun onBeginAuth() = Unit

    override fun onAuthenticated(user: User) {
        GlideApp.with(context)
                .load(user.photoUrl)
                .apply(RequestOptions.circleCropTransform())
                .into(ivAvatar)
    }

    override fun onNotActivated(user: User) = onAuthenticated(user)

    override fun onFailureAuth(message: String) = Unit

}