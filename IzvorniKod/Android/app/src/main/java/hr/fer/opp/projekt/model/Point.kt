package hr.fer.opp.projekt.model

import java.io.Serializable

data class Point(
        val lat: Double = 0.toDouble(),
        val lng: Double = 0.toDouble()
) : Serializable