package hr.fer.opp.projekt.util.listeners

import hr.fer.opp.projekt.model.User

interface AuthListener {
    fun onBeginAuth()
    fun onAuthenticated(user: User)
    fun onNotActivated(user: User)
    fun onFailureAuth(message: String)
}