package hr.fer.opp.projekt.fragments


import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineDataSet
import hr.fer.opp.projekt.R
import hr.fer.opp.projekt.controllers.StatisticsFragmentController
import hr.fer.opp.projekt.model.Block

class BlockStatisticsFragment : StatisticsFragmentController() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? =
            inflater.inflate(R.layout.fragment_block_statistics, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val blockFinishDataset = LineDataSet(ArrayList<Entry>(), "Blocks finished")
        blockFinishDataset.color = Color.GREEN
        blockFinishDataset.setGradientColor(Color.GREEN, Color.BLUE)
        blockFinishDataset.valueFormatter = DayAxisValueFormatter(timeReference)
        blockFinishDataset.setDrawHighlightIndicators(false)
        blockFinishDataset.setDrawFilled(true)
        val drawable = ContextCompat.getDrawable(context!!, R.drawable.green_blue_gradient)
        blockFinishDataset.fillDrawable = drawable

        setChartData(blockFinishDataset)
        markerSetup("blocks finished")

        db.collection("blocks").get().addOnSuccessListener {
            val blocks = it.toObjects(Block::class.java)
            val dates = blocks.mapNotNull { block -> block.finishDate }.sorted()
            fillChart(dates, blockFinishDataset)
        }
    }

}
