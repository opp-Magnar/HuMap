package hr.fer.opp.projekt.util.listeners

import hr.fer.opp.projekt.model.User

interface RegistrationListener {
    fun onBeginRegistration()
    fun onRegistered(user: User)
    fun onFailureRegistration(message: String)
}