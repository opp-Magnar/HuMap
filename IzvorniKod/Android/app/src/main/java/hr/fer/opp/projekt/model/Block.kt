package hr.fer.opp.projekt.model


import com.google.firebase.firestore.DocumentId
import java.io.Serializable
import java.util.*

data class Block(
        @DocumentId val documentId: String = "",
        var uid: String? = null,
        val polygon: List<Point> = arrayListOf(),
        var state: BlockState = BlockState.INACTIVE,
        var finishDate: Date? = null
) : Serializable {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Block

        if (documentId != other.documentId) return false

        return true
    }

    override fun hashCode(): Int {
        return documentId.hashCode()
    }
}