package hr.fer.opp.projekt.model

import com.google.firebase.firestore.DocumentId
import java.io.Serializable

data class Region(
        @DocumentId val documentId: String = "",
        val polygon: List<Point> = arrayListOf()
) : Serializable {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Region

        if (documentId != other.documentId) return false

        return true
    }

    override fun hashCode(): Int {
        return documentId.hashCode()
    }
}