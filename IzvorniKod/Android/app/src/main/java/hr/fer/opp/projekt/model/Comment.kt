package hr.fer.opp.projekt.model

import java.io.Serializable
import java.util.*

data class Comment(
    val userUid: String? = null,
    val comment: String = "",
    val date: Date = Date()
) : Serializable