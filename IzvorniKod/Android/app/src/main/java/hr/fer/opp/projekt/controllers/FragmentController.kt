package hr.fer.opp.projekt.controllers


import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.annotation.IdRes
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.NavigationUI
import com.google.common.collect.ImmutableList
import hr.fer.opp.projekt.R
import hr.fer.opp.projekt.model.LoginViewModel
import hr.fer.opp.projekt.model.LoginViewModel.AuthenticationState
import hr.fer.opp.projekt.model.Role
import hr.fer.opp.projekt.util.listeners.DatabaseListener
import hr.fer.opp.projekt.util.listeners.OnFragmentInteractionListener

abstract class FragmentController : Fragment(), DatabaseListener, Observer<AuthenticationState> {

    protected val authenticatedUserAppBarStart: ImmutableList<Int> = ImmutableList.of(R.id.menuSignOut, R.id.menuEditProfile)
    protected val authenticatedUserAppBar: MutableList<Int> = mutableListOf(R.id.menuSignOut, R.id.menuEditProfile)
    private val unauthenticatedUserAppBar: ImmutableList<Int> = ImmutableList.of()

    private val adminBottomNavBar: ImmutableList<Int> = ImmutableList.of(R.id.reports_nav, R.id.map_nav, R.id.stats_nav, R.id.users_nav)
    private val highAccessBottomNavBar: ImmutableList<Int> = ImmutableList.of(R.id.reports_nav, R.id.map_nav, R.id.stats_nav)
    private val lowAccessBottomNavBar: ImmutableList<Int> = ImmutableList.of(R.id.reports_nav, R.id.map_nav)
    private val unauthenticatedBottomNavBar: ImmutableList<Int> = ImmutableList.of(R.id.reports_nav, R.id.auth_nav)

    private var visibleAppBarMenuItems: List<Int> = unauthenticatedUserAppBar
    private var visibleBottomNavBarMenuItems: ImmutableList<Int> = unauthenticatedBottomNavBar
    private var currentRole: Role? = null
    private lateinit var toolbar: Toolbar
    private val navController: NavController by lazy { findNavController() }

    protected val loginViewModel: LoginViewModel by lazy { ViewModelProviders.of(requireActivity()).get(LoginViewModel::class.java) }
    protected lateinit var mListener: OnFragmentInteractionListener

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        currentRole = if (loginViewModel.currentUser != null) loginViewModel.currentUser!!.role else null
        toolbar = view.findViewById(R.id.fragmentToolbar)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        mListener = if (context is OnFragmentInteractionListener) {
            context
        } else {
            throw RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onResume() {
        super.onResume()
        registerListener()
        toolbarSetup()
    }

    override fun onPause() {
        super.onPause()
        removeListener()
    }

    private fun toolbarSetup() {
        (activity as AppCompatActivity).setSupportActionBar(toolbar)
        NavigationUI.setupActionBarWithNavController(activity as AppCompatActivity, navController)
    }

    protected fun navigateTo(@IdRes action: Int, bundle: Bundle = Bundle()) =
            navController.navigate(action, bundle)

    protected fun back() {
        navController.popBackStack()
    }

    protected fun userChanged(): Boolean {
        val currUser = loginViewModel.currentUser
        if (currUser == null) {
            visibleAppBarMenuItems = unauthenticatedUserAppBar
            visibleBottomNavBarMenuItems = unauthenticatedBottomNavBar
        } else {
            visibleAppBarMenuItems = authenticatedUserAppBar
            visibleBottomNavBarMenuItems = when (currUser.role) {
                Role.ADMIN -> adminBottomNavBar
                Role.VODITELJ, Role.KARTOGRAF -> highAccessBottomNavBar
                Role.SPASIOC -> lowAccessBottomNavBar
            }
        }
        showBottomNavBar()
        showAppBar()
        return if (currUser == null && currentRole == null || currUser != null && currentRole != null && currUser.role == currentRole) {
            false
        } else {
            currentRole = currUser?.role
            true
        }
    }

    private fun showAppBar() = mListener.showAppBarItems(visibleAppBarMenuItems)

    private fun showBottomNavBar() = mListener.showBottomNavBarItems(visibleBottomNavBarMenuItems)

    private fun registerListener() = loginViewModel.authenticationState.observe(viewLifecycleOwner, this)

    private fun removeListener() = loginViewModel.authenticationState.removeObserver(this)

    override fun onBegin() = mListener.begin()

    override fun onSuccess() = mListener.stop()

    override fun onFailure(message: String) {
        mListener.stop()
        mListener.showSnackbar(message)
    }

    override fun onChanged(authState: AuthenticationState?) {
        if (authState != AuthenticationState.AUTHENTICATED) {
            mListener.navigateBottomAppBarReset(R.id.auth_nav)
        } else {
            if (userChanged()) {
                mListener.navigateBottomAppBarReset(R.id.reports_nav)
            }
        }
    }

}