package hr.fer.opp.projekt.util.listeners


interface OnFragmentInteractionListener {
    fun begin()
    fun stop()
    fun showSnackbar(text: String)
    fun showBottomNavBarItems(visibleMenuItems: List<Int>)
    fun showBottomNavBar()
    fun hideBottomNavBar()
    fun showAppBarItems(visibleMenuItems: List<Int>)
    fun navigateBottomAppBar(id: Int)
    fun navigateBottomAppBarReset(id: Int)
}