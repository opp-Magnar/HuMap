package hr.fer.opp.projekt.util.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import com.firebase.ui.firestore.FirestoreRecyclerAdapter
import com.firebase.ui.firestore.FirestoreRecyclerOptions
import hr.fer.opp.projekt.R
import hr.fer.opp.projekt.model.LoginViewModel
import hr.fer.opp.projekt.model.Report
import hr.fer.opp.projekt.util.holders.ReportHolder
import hr.fer.opp.projekt.util.listeners.RecyclerViewOnClickListener

class FirestoreAdapterReport(options: FirestoreRecyclerOptions<Report>, private val context: Context,
                             private val listener: RecyclerViewOnClickListener,
                             private val loginViewModel: LoginViewModel) : FirestoreRecyclerAdapter<Report, ReportHolder>(options) {

    override fun onBindViewHolder(holder: ReportHolder, position: Int, model: Report) =
            holder.setReport(model, position)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ReportHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.card_item, parent, false)
        return ReportHolder(view, listener, context, loginViewModel)
    }

}