package hr.fer.opp.projekt.model

import java.io.Serializable

enum class Role : Serializable {
    ADMIN, VODITELJ, KARTOGRAF, SPASIOC
}
