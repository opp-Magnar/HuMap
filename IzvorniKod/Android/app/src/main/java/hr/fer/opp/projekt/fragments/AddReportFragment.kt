package hr.fer.opp.projekt.fragments


import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.theartofdev.edmodo.cropper.CropImage
import com.theartofdev.edmodo.cropper.CropImageView
import hr.fer.opp.projekt.R
import hr.fer.opp.projekt.controllers.FragmentController
import hr.fer.opp.projekt.model.LoginViewModel
import hr.fer.opp.projekt.model.Report
import kotlinx.android.synthetic.main.fragment_add_report.*

class AddReportFragment : FragmentController() {

    private var imageUri: Uri? = null
    private lateinit var ivPhoto: ImageView

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? =
            inflater.inflate(R.layout.fragment_add_report, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        ivPhoto = view.findViewById(R.id.ivPhoto)
        ivPhoto.setImageResource(R.drawable.photo)
        fabAddReportAddPhoto.setOnClickListener {
            CropImage.activity()
                    .setGuidelines(CropImageView.Guidelines.ON)
                    .start(context!!, this)
        }
        btnReportCreate.setOnClickListener {
            val report = validateUserInput() ?: return@setOnClickListener
            loginViewModel.createReport(this, report, imageUri)
        }
    }

    private fun clearErrors() {
        tiAddReportFirstName!!.error = null
        tiAddReportLastName!!.error = null
        tiAddReportDescription!!.error = null
    }

    private fun validateUserInput(): Report? {
        clearErrors()
        val firstName = etAddReportFirstName.text.toString()
        val lastName = etAddReportLastName.text.toString()
        val description = etAddReportDescription.text.toString()
        if (firstName.isEmpty() || lastName.isEmpty()) {
            if (firstName.isEmpty()) {
                tiAddReportFirstName.error = "This field is required"
            }
            if (lastName.isEmpty()) {
                tiAddReportLastName.error = "This field is required"
            }
            return null
        }
        return Report(
                firstName = firstName,
                lastName = lastName,
                description = description,
                photoUrl = imageUri?.toString(),
                userUid = loginViewModel.currentUser?.uid)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            val result = CropImage.getActivityResult(data)
            if (resultCode == Activity.RESULT_OK) {
                imageUri = result!!.uri
                ivPhoto.setImageURI(imageUri)
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                val error = result!!.error
                onFailure(error.message!!)
            }
        }
    }

    override fun onChanged(authState: LoginViewModel.AuthenticationState?) {
        userChanged()
    }

    override fun onSuccess() {
        super.onSuccess()
        back()
    }

}
