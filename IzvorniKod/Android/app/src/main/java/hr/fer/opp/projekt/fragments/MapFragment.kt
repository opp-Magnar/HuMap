package hr.fer.opp.projekt.fragments


import android.app.Dialog
import android.graphics.Color
import android.os.Bundle
import android.view.*
import androidx.core.view.isVisible
import androidx.fragment.app.DialogFragment
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Polygon
import com.google.android.gms.maps.model.PolygonOptions
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.firebase.firestore.DocumentChange
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.ListenerRegistration
import hr.fer.opp.projekt.R
import hr.fer.opp.projekt.controllers.FragmentController
import hr.fer.opp.projekt.model.*
import kotlinx.android.synthetic.main.fragment_map.*
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

fun canAddRegions(currentUser: User?): Boolean =
        currentUser != null && currentUser.role == Role.ADMIN || currentUser?.role == Role.VODITELJ

fun canAddBlocks(currentUser: User?): Boolean =
        currentUser != null && currentUser.role == Role.ADMIN || currentUser?.role == Role.VODITELJ

fun canAddBuildings(currentUser: User?): Boolean =
        currentUser != null && currentUser.role == Role.ADMIN || currentUser?.role == Role.VODITELJ ||
                currentUser?.role == Role.KARTOGRAF

fun canChangeBlockState(currentUser: User?): Boolean =
        currentUser != null && currentUser.role == Role.ADMIN || currentUser?.role == Role.KARTOGRAF

fun canChangeBuildingState(currentUser: User?): Boolean =
        currentUser != null && currentUser.role == Role.ADMIN || currentUser?.role == Role.SPASIOC

fun canSeeRegionsAndBlocks(currentUser: User?): Boolean =
        currentUser != null && currentUser.role == Role.ADMIN || currentUser?.role == Role.VODITELJ ||
                currentUser?.role == Role.KARTOGRAF

class MapFragment : FragmentController(), OnMapReadyCallback, GoogleMap.OnMapClickListener {

    enum class AreaType {
        REGION, BLOCK, BUILDING
    }

    companion object {
        private val regionColor = Color.argb(100, 150, 75, 0)
        private val blockColorInactive = Color.argb(100, 255, 0, 0)
        private val blockColorActive = Color.argb(100, 255, 165, 0)
        private val blockColorReview = Color.argb(100, 255, 255, 0)
        private val blockColorFinished = Color.argb(100, 0, 255, 0)
        private val blockColorReserved = Color.argb(100, 0, 0, 0)
        private val buildingColorUnsearched = Color.argb(100, 0, 0, 255)
        private val buildingColorSearched = Color.argb(100, 255, 255, 255)
        private var hasActive = false
    }

    private val db by lazy { FirebaseFirestore.getInstance() }

    private val regionPolygon = HashMap<Region, Polygon>()
    private val polygonBlock = HashMap<Polygon, Block>()
    private val polygonBuilding = HashMap<Polygon, Building>()

    private var actionMode: ActionMode? = null

    private var currentAreaType = AreaType.REGION
    private var currentStrokeColor = Color.TRANSPARENT
    private var currentFillColor = regionColor
    private var marking = false

    private val currentPolygonPoints = ArrayList<LatLng>()
    private var lastPolygon: Polygon? = null
    private var regionListenerRegistration: ListenerRegistration? = null
    private var blockListenerRegistration: ListenerRegistration? = null
    private var buildingListenerRegistration: ListenerRegistration? = null

    private var mMap: GoogleMap? = null

    private val areaOnClick = View.OnClickListener { v ->
        when (v.id) {
            R.id.regionOption -> startRegion()
            R.id.blockOption -> startBlock()
            R.id.buildingOption -> startBuilding()
        }
        fabAddArea.isExpanded = false
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? =
            inflater.inflate(R.layout.fragment_map, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        fabAddArea.setOnClickListener { fabAddArea.isExpanded = true }
        scrim.setOnClickListener { fabAddArea.isExpanded = false }
        regionOption.setOnClickListener(areaOnClick)
        blockOption.setOnClickListener(areaOnClick)
        buildingOption.setOnClickListener(areaOnClick)
    }

    override fun onStart() {
        super.onStart()
        val mapFragment = (childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment?)!!
        mapFragment.getMapAsync(this)
    }

    override fun onMapReady(googleMap: GoogleMap) {
        if (mMap == null) {
            val zagreb = LatLng(45.81, 15.98)
            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(zagreb, 15.0f))
            setRegionCollectionListener()
            setBlockCollectionListener()
            setBuildingCollectionListener()
        }
        mMap = googleMap
        googleMap.uiSettings.isRotateGesturesEnabled = false
        googleMap.setOnMapClickListener(this)
        setOnPolygonClickListener()
    }

    override fun onMapClick(latlng: LatLng) {
        if (!marking) return
        lastPolygon?.remove()
        currentPolygonPoints.add(latlng)
        lastPolygon = drawPolygon(currentPolygonPoints, currentStrokeColor, currentFillColor)
    }

    private fun drawRegion(region: Region) {
        drawPolygonPoints(region.polygon, regionColor, Color.TRANSPARENT).apply {
            regionPolygon[region] = this
        }
    }

    private fun drawBlock(block: Block, clickable: Boolean) {
        val color = getBlockColor(block)
        drawPolygonPoints(block.polygon, color, color).apply {
            this.isClickable = clickable
            polygonBlock[this] = block
        }
    }

    private fun drawBuilding(building: Building, clickable: Boolean) {
        val color = getBuildingColor(building)
        drawPolygonPoints(building.polygon, color, color).apply {
            this.isClickable = clickable
            polygonBuilding[this] = building
        }
    }

    private fun getBlockColor(block: Block): Int = when (block.state) {
        BlockState.INACTIVE -> blockColorInactive
        BlockState.ACTIVE -> {
            if (block.uid!! == loginViewModel.currentUser!!.uid) blockColorActive
            else blockColorReserved
        }
        BlockState.REVIEW -> blockColorReview
        BlockState.FINISHED -> blockColorFinished
    }

    private fun getBuildingColor(building: Building): Int = when (building.state) {
        BuildingState.SEARCHED -> buildingColorSearched
        BuildingState.UNSEARCHED -> buildingColorUnsearched
    }

    private fun clickable() {
        polygonBlock.forEach {
            it.key.isClickable = isBlockClickable(it.value)
        }
        polygonBuilding.forEach {
            it.key.isClickable = isBuildingClickable()
        }
    }

    private fun drawPolygonPoints(polygon: List<Point>, strokeColor: Int, fillColor: Int): Polygon =
            drawPolygon(polygon.map { LatLng(it.lat, it.lng) }, strokeColor, fillColor)

    private fun drawPolygon(polygon: List<LatLng>, strokeColor: Int, fillColor: Int): Polygon = with(PolygonOptions()) {
        addAll(polygon)
        strokeColor(strokeColor)
        fillColor(fillColor)
        strokeWidth(6f)
        return mMap!!.addPolygon(this)
    }

    private fun startMarking(): ActionMode =
            activity!!.startActionMode(actionModeCallback)!!.also {
                actionMode = it
                marking = true
                clickable()
            }

    private fun stopMarking() {
        marking = false
        clearCurrentPolygon()
        clickable()
    }

    private fun startRegion() {
        startMarking().title = "Region"
        currentAreaType = AreaType.REGION
        currentStrokeColor = regionColor
        currentFillColor = Color.TRANSPARENT
    }

    private fun startBlock() {
        startMarking().title = "Block"
        currentAreaType = AreaType.BLOCK
        currentStrokeColor = blockColorInactive
        currentFillColor = blockColorInactive
    }

    private fun startBuilding() {
        startMarking().title = "Building"
        currentAreaType = AreaType.BUILDING
        currentStrokeColor = buildingColorUnsearched
        currentFillColor = buildingColorUnsearched
    }

    private fun save() {
        if (currentPolygonPoints.size < 3) return
        onBegin()
        when (currentAreaType) {
            AreaType.REGION -> saveRegion()
            AreaType.BLOCK -> saveBlock()
            AreaType.BUILDING -> saveBuilding()
        }
    }

    private fun saveRegion() {
        val region = Region(polygon = currentPolygonPoints.map { Point(it.latitude, it.longitude) })
        db.collection("regions").add(region)
                .addOnSuccessListener {
                    actionMode?.finish()
                    onSuccess()
                }
                .addOnFailureListener {
                    onFailure(it.message!!)
                }
    }

    private fun saveBlock() {
        val block = Block(polygon = currentPolygonPoints.map { Point(it.latitude, it.longitude) })
        db.collection("blocks").add(block)
                .addOnSuccessListener {
                    actionMode?.finish()
                    onSuccess()
                }
                .addOnFailureListener {
                    onFailure(it.message!!)
                }
    }

    private fun saveBuilding() {
        val building = Building(polygon = currentPolygonPoints.map { Point(it.latitude, it.longitude) })
        db.collection("buildings").add(building)
                .addOnSuccessListener {
                    actionMode?.finish()
                    onSuccess()
                }
                .addOnFailureListener {
                    onFailure(it.message!!)
                }
    }

    private fun isBlockClickable(block: Block): Boolean = if (!canChangeBlockState(loginViewModel.currentUser)) false
    else if (block.state == BlockState.INACTIVE || block.state == BlockState.FINISHED || block.state == BlockState.REVIEW) {
        true
    } else block.state == BlockState.ACTIVE && (block.uid!! != loginViewModel.currentUser!!.uid || !marking)

    private fun isBuildingClickable(): Boolean = canChangeBuildingState(loginViewModel.currentUser) && !marking

    private fun clearCurrentPolygon() {
        currentPolygonPoints.clear()
        lastPolygon?.remove()
    }

    private val actionModeCallback = object : ActionMode.Callback {
        override fun onActionItemClicked(mode: ActionMode?, item: MenuItem): Boolean {
            when (item.itemId) {
                R.id.menuSave -> return true.apply { save() }
                R.id.menuDelete -> return true.apply { clearCurrentPolygon() }
            }
            return false
        }

        override fun onCreateActionMode(mode: ActionMode?, menu: Menu?): Boolean =
                true.apply { mode!!.menuInflater.inflate(R.menu.contextual_action_mode_menu, menu) }

        override fun onPrepareActionMode(mode: ActionMode?, menu: Menu?): Boolean = false

        override fun onDestroyActionMode(mode: ActionMode?) = stopMarking()
    }

    private fun setRegionCollectionListener() {
        regionListenerRegistration?.remove()
        regionListenerRegistration = db.collection("regions").addSnapshotListener { snapshots, e ->
            if (e != null || !canSeeRegionsAndBlocks(loginViewModel.currentUser)) return@addSnapshotListener
            for (dc in snapshots!!.documentChanges) {
                val region = dc.document.toObject(Region::class.java)
                when (dc.type) {
                    DocumentChange.Type.ADDED -> {
                        drawRegion(region)
                    }
                    DocumentChange.Type.REMOVED -> {
                        regionPolygon[region]!!.remove()
                        regionPolygon.remove(region)
                    }
                    else -> Unit
                }
            }
        }
    }

    private fun setBlockCollectionListener() {
        blockListenerRegistration?.remove()
        blockListenerRegistration = db.collection("blocks").addSnapshotListener { snapshots, e ->
            if (e != null || !canSeeRegionsAndBlocks(loginViewModel.currentUser)) return@addSnapshotListener
            for (dc in snapshots!!.documentChanges) {
                val block = dc.document.toObject(Block::class.java)
                val clickable = isBlockClickable(block)
                hasActive = hasActive || block.state == BlockState.ACTIVE && block.uid!! == loginViewModel.currentUser!!.uid
                when (dc.type) {
                    DocumentChange.Type.ADDED -> drawBlock(block, clickable)
                    DocumentChange.Type.MODIFIED -> {
                        val color = getBlockColor(block)
                        polygonBlock.filter { it.value == block }.forEach {
                            it.key.fillColor = color
                            it.key.strokeColor = color
                            it.key.isClickable = clickable
                            polygonBlock[it.key] = block
                        }
                    }
                    DocumentChange.Type.REMOVED -> {
                        polygonBlock.filter { it.value == block }.forEach {
                            it.key.remove()
                        }
                        polygonBlock.values.remove(block)
                    }
                }
            }
        }
    }

    private fun setBuildingCollectionListener() {
        buildingListenerRegistration?.remove()
        buildingListenerRegistration = db.collection("buildings").addSnapshotListener { snapshots, e ->
            if (e != null) {
                return@addSnapshotListener
            }
            for (dc in snapshots!!.documentChanges) {
                val building = dc.document.toObject(Building::class.java)
                val clickable = isBuildingClickable()
                when (dc.type) {
                    DocumentChange.Type.ADDED -> drawBuilding(building, clickable)
                    DocumentChange.Type.MODIFIED -> {
                        val color = getBuildingColor(building)
                        polygonBuilding.filter { it.value == building }.forEach {
                            it.key.fillColor = color
                            it.key.strokeColor = color
                            it.key.isClickable = clickable
                            polygonBuilding[it.key] = building
                        }
                    }
                    DocumentChange.Type.REMOVED -> {
                        polygonBuilding.filter { it.value == building }.forEach {
                            it.key.remove()
                        }
                        polygonBuilding.values.remove(building)
                    }
                }
            }
        }
    }

    private fun setOnPolygonClickListener() = mMap!!.setOnPolygonClickListener {
        if (polygonBlock.containsKey(it)) {
            blockClicked(it)
        } else {
            buildingClicked(it)
        }
    }

    private fun blockClicked(it: Polygon) {
        val nextStates = arrayListOf<String>()
        val block = polygonBlock[it]!!
        when (block.state) {
            BlockState.INACTIVE -> if (!hasActive) {
                nextStates.add("ACTIVE")
            }
            BlockState.ACTIVE -> {
                if (block.uid!! != loginViewModel.currentUser!!.uid) {
                    return
                }
                nextStates.add("REVIEW")
            }
            BlockState.REVIEW -> {
                if (!hasActive) {
                    nextStates.add("ACTIVE")
                }
                if (block.uid!! != loginViewModel.currentUser!!.uid) {
                    nextStates.add("FINISHED")
                }
            }
            else -> return
        }
        if (nextStates.isEmpty()) return
        BlockStateDialogFragment(nextStates, it, polygonBlock[it]!!, db, loginViewModel.currentUser!!.uid)
                .show(activity!!.supportFragmentManager, "Dialog")
    }

    private fun buildingClicked(it: Polygon) {
        val nextStates = arrayListOf<String>()
        val building = polygonBuilding[it]!!
        when (building.state) {
            BuildingState.UNSEARCHED -> nextStates.add("SEARCHED")
            BuildingState.SEARCHED -> nextStates.add("UNSEARCHED")
        }
        BuildingStateDialogFragment(nextStates, it, polygonBuilding[it]!!, db)
                .show(activity!!.supportFragmentManager, "Dialog")
    }

    override fun onChanged(authState: LoginViewModel.AuthenticationState?) {
        super.onChanged(authState)
        if (authState == LoginViewModel.AuthenticationState.AUTHENTICATED) {
            fabAddArea.isVisible = canAddRegions(loginViewModel.currentUser) || canAddBlocks(loginViewModel.currentUser) || canAddBuildings(loginViewModel.currentUser)
            regionOption.isVisible = canAddRegions(loginViewModel.currentUser)
            blockOption.isVisible = canAddBlocks(loginViewModel.currentUser)
            buildingOption.isVisible = canAddBuildings(loginViewModel.currentUser)
        }
    }

    class BlockStateDialogFragment(private val nextStates: ArrayList<String>, private val polygon: Polygon, private val block: Block,
                                                 private val db: FirebaseFirestore, private val uid: String) : DialogFragment() {

        override fun onCreateDialog(
                savedInstanceState: Bundle?
        ): Dialog = MaterialAlertDialogBuilder(requireContext())
                .setTitle("Change block state")
                .setItems(nextStates.toTypedArray()) { _, which ->
                    when (nextStates[which]) {
                        "ACTIVE" -> setBlockStateActive()
                        "FINISHED" -> setBlockStateFinished()
                        "REVIEW" -> setBlockStateReview()
                    }
                    db.collection("blocks").document(block.documentId).set(block)
                }
                .create()

        private fun setBlockStateActive() {
            polygon.fillColor = blockColorActive
            polygon.strokeColor = blockColorActive
            block.state = BlockState.ACTIVE
            block.uid = uid
        }

        private fun setBlockStateFinished() {
            polygon.fillColor = blockColorFinished
            polygon.strokeColor = blockColorFinished
            block.state = BlockState.FINISHED
            block.finishDate = Date()
        }

        private fun setBlockStateReview() {
            polygon.fillColor = blockColorReview
            polygon.strokeColor = blockColorReview
            block.state = BlockState.REVIEW
            hasActive = false
        }

    }

    class BuildingStateDialogFragment(private val nextState: ArrayList<String>, private val polygon: Polygon, private val building: Building,
                                              private val db: FirebaseFirestore) : DialogFragment() {

        override fun onCreateDialog(
                savedInstanceState: Bundle?
        ): Dialog = MaterialAlertDialogBuilder(requireContext())
                .setTitle("Change building state")
                .setItems(nextState.toTypedArray()) { _, which ->
                    when (nextState[which]) {
                        "SEARCHED" -> setBuildingStateSearched()
                        "UNSEARCHED" -> setBuildingStateUnsearched()
                    }
                    db.collection("buildings").document(building.documentId).set(building)
                }
                .create()

        private fun setBuildingStateSearched() {
            polygon.fillColor = buildingColorSearched
            polygon.strokeColor = buildingColorSearched
            building.state = BuildingState.SEARCHED
            building.finishDate = Date()
        }

        private fun setBuildingStateUnsearched() {
            polygon.fillColor = buildingColorUnsearched
            polygon.strokeColor = buildingColorUnsearched
            building.state = BuildingState.UNSEARCHED
            building.finishDate = null
        }

    }

}
