package hr.fer.opp.projekt.util.adapters


import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import hr.fer.opp.projekt.fragments.BlockStatisticsFragment
import hr.fer.opp.projekt.fragments.BuildingStatisticsFragment
import hr.fer.opp.projekt.fragments.ReportStatisticsFragment

class PageAdapter(fragmentManager: FragmentManager, private val numberOfTabs: Int)
    : FragmentPagerAdapter(fragmentManager, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    override fun getItem(position: Int): Fragment = when (position) {
        0 -> ReportStatisticsFragment()
        1 -> BlockStatisticsFragment()
        2 -> BuildingStatisticsFragment()
        else -> throw IllegalStateException()
    }

    override fun getPageTitle(position: Int) = when (position) {
        0 -> "Reports"
        1 -> "Blocks"
        2 -> "Buildings"
        else -> ""
    }

    override fun getCount(): Int {
        return numberOfTabs
    }

}