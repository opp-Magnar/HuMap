package hr.fer.opp.projekt.util

import android.net.Uri
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.*
import com.google.firebase.firestore.EventListener
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.UploadTask
import hr.fer.opp.projekt.model.Report
import hr.fer.opp.projekt.model.Role
import hr.fer.opp.projekt.model.User
import hr.fer.opp.projekt.util.listeners.AuthListener
import hr.fer.opp.projekt.util.listeners.DatabaseListener
import hr.fer.opp.projekt.util.listeners.RegistrationListener
import java.util.*

object FirebaseSource {

    private val mAuth = FirebaseAuth.getInstance()
    private val db = FirebaseFirestore.getInstance()
    private val storageReference = FirebaseStorage.getInstance().reference

    fun authenticate(listener: AuthListener, email: String, password: String) {
        listener.onBeginAuth()
        mAuth.signInWithEmailAndPassword(email, password)
                .addOnSuccessListener { getCurrentUserObjectAsync(listener) }
                .addOnFailureListener { exception: Exception -> listener.onFailureAuth(exception.localizedMessage!!) }
    }

    fun createUser(listener: RegistrationListener, user: User, password: String, imageUri: Uri) {
        Objects.requireNonNull(imageUri)
        listener.onBeginRegistration()
        db.collection("users")
                .whereEqualTo("username", user.username)
                .get()
                .addOnSuccessListener { querySnapshot: QuerySnapshot ->
                    if (querySnapshot.documents.isEmpty()) {
                        create(listener, user, password, imageUri)
                    } else {
                        listener.onFailureRegistration("Username already exists!")
                    }
                }.addOnFailureListener { exception: Exception -> listener.onFailureRegistration(exception.localizedMessage!!) }
    }

    fun changeUserRole(listener: DatabaseListener?, user: User, role: Role) {
        listener?.onBegin()
        user.role = role
        db.collection("users").document(user.uid).set(user).addOnSuccessListener {
            listener?.onSuccess()
        }.addOnFailureListener { exception ->
            listener?.onFailure(exception.localizedMessage!!)
        }
    }

    fun createReport(listener: DatabaseListener, report: Report, imageUri: Uri?) {
        listener.onBegin()
        if (imageUri != null) {
            val imagePath = storageReference.child("report_images")
            imagePath.putFile(imageUri).continueWithTask { task: Task<UploadTask.TaskSnapshot?> ->
                if (!task.isSuccessful) {
                    throw task.exception!!
                }
                imagePath.downloadUrl
            }.addOnSuccessListener { uri: Uri ->
                report.photoUrl = uri.toString()
                saveReport(listener, report)
            }.addOnFailureListener { exception1: Exception -> listener.onFailure(exception1.localizedMessage!!) }
        } else {
            saveReport(listener, report)
        }
    }

    private fun saveReport(listener: DatabaseListener, report: Report) {
        db.collection("reports").add(report)
                .addOnSuccessListener { listener.onSuccess() }
                .addOnFailureListener { exception2: Exception -> listener.onFailure(exception2.localizedMessage!!) }
    }

    fun getCurrentUserObjectAsync(listener: AuthListener) {
        val firebaseUser = mAuth.currentUser
        if (firebaseUser == null) {
            listener.onFailureAuth("User is not logged in!")
            return
        }
        getUserObjectAsync(firebaseUser.uid, listener)
    }

    fun getUserObjectAsync(userUid: String, listener: AuthListener) {
        val docRef = db.collection("users").document(userUid)
        docRef.get().addOnSuccessListener { documentSnapshot: DocumentSnapshot ->
            if (documentSnapshot.exists()) {
                val user = documentSnapshot.toObject(User::class.java)!!
                if (user.active) {
                    listener.onAuthenticated(user)
                } else {
                    listener.onNotActivated(user)
                }
            } else {
                listener.onFailureAuth("User is not saved in database. Contact admin for more information.")
            }
        }.addOnFailureListener { exception: Exception -> listener.onFailureAuth(exception.localizedMessage!!) }
    }

    private fun create(listener: RegistrationListener, user: User, password: String, imageUri: Uri) {
        mAuth.createUserWithEmailAndPassword(user.email, password)
                .addOnSuccessListener {
                    val currUser = mAuth.currentUser
                    if (currUser == null) {
                        listener.onFailureRegistration("User is null!")
                        return@addOnSuccessListener
                    }
                    val imagePath = storageReference.child("profile_images").child(currUser.uid)
                    imagePath.putFile(imageUri).continueWithTask { task: Task<UploadTask.TaskSnapshot?> ->
                        if (!task.isSuccessful) {
                            throw task.exception!!
                        }
                        imagePath.downloadUrl
                    }.addOnSuccessListener { uri: Uri ->
                        user.photoUrl = uri.toString()
                        db.collection("users").document(currUser.uid).set(user)
                                .addOnSuccessListener { listener.onRegistered(user) }
                                .addOnFailureListener { exception3: Exception -> listener.onFailureRegistration("Exception3: " + exception3.localizedMessage) }
                    }.addOnFailureListener { exception2: Exception -> listener.onFailureRegistration("Exception2: " + exception2.localizedMessage) }
                }.addOnFailureListener { exception1: Exception -> listener.onFailureRegistration("Exception1: " + exception1.localizedMessage) }
    }

    fun saveReportChanges(report: Report) {
        db.collection("reports").document(report.documentId).set(report)
    }

    fun addUserDocumentListener(listener: EventListener<DocumentSnapshot?>, uid: String): ListenerRegistration {
        val docRef = db.collection("users").document(uid)
        return docRef.addSnapshotListener(listener)
    }

    fun activateUser(listener: DatabaseListener?, user: User) {
        listener?.onBegin()
        user.active = true
        db.collection("users").document(user.uid).set(user)
                .addOnSuccessListener { listener?.onSuccess() }
                .addOnFailureListener { exception: Exception -> listener?.onFailure(exception.localizedMessage!!) }
    }

    fun signOut() = mAuth.signOut()

}