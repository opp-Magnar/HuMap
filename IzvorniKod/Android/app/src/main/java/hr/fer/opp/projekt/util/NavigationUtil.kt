package hr.fer.opp.projekt.util


import android.content.Intent
import android.util.SparseArray
import android.util.SparseIntArray
import androidx.core.util.forEach
import androidx.core.util.set
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import java.lang.IllegalArgumentException

var reset = false

fun BottomNavigationView.setupWithNavController(
        navGraphIds: List<Int>,
        fragmentManager: FragmentManager,
        containerId: Int,
        intent: Intent
): LiveData<NavController> {

    val graphIdToTagMap = SparseArray<String>()
    val graphIdToNavGraphId = SparseIntArray()
    val selectedNavController = MutableLiveData<NavController>()
    val firstFragmentTag: String = getFragmentTag(0)

    navGraphIds.forEachIndexed { index, navGraphId ->
        val fragmentTag = getFragmentTag(index)
        val navHostFragment = obtainNavHostFragment(
                fragmentManager,
                fragmentTag,
                navGraphId,
                containerId
        )
        val graphId = navHostFragment.navController.graph.id
        graphIdToTagMap[graphId] = fragmentTag
        graphIdToNavGraphId[graphId] = navGraphId

        if (this.selectedItemId == graphId) {
            selectedNavController.value = navHostFragment.navController
            attachNavHostFragment(fragmentManager, navHostFragment, index == 0)
        } else {
            detachNavHostFragment(fragmentManager, navHostFragment)
        }
    }

    var selectedItemTag = graphIdToTagMap[this.selectedItemId]

    setOnNavigationItemSelectedListener { item ->
        if (fragmentManager.isStateSaved) {
            false
        } else {
            val newlySelectedItemTag = graphIdToTagMap[item.itemId]
            if (selectedItemTag != newlySelectedItemTag) {
                val selectedFragment = obtainNavHostFragment(
                        fragmentManager,
                        newlySelectedItemTag,
                        graphIdToNavGraphId[item.itemId],
                        containerId
                )
                fragmentManager.beginTransaction()
                        .attach(selectedFragment)
                        .setPrimaryNavigationFragment(selectedFragment)
                        .apply {
                            // Detach all other Fragments
                            graphIdToTagMap.forEach { _, fragmentTagIter ->
                                if (fragmentTagIter != newlySelectedItemTag) {
                                    fragmentManager.findFragmentByTag(fragmentTagIter)?.let { detach(it) }
                                }
                            }
                        }
                        .apply {
                            if (reset) {
                                clearFragmentManager(this, fragmentManager, navGraphIds, firstFragmentTag, newlySelectedItemTag)
                            }
                        }
                        .setReorderingAllowed(true)
                        .commit()
                reset = false
                selectedItemTag = newlySelectedItemTag
                selectedNavController.value = selectedFragment.navController
                true
            } else {
                false
            }
        }
    }

    setupDeepLinks(navGraphIds, fragmentManager, containerId, intent)

    return selectedNavController
}

private fun BottomNavigationView.setupDeepLinks(
        navGraphIds: List<Int>,
        fragmentManager: FragmentManager,
        containerId: Int,
        intent: Intent
) = navGraphIds.forEachIndexed { index, navGraphId ->
    val fragmentTag = getFragmentTag(index)
    val navHostFragment = obtainNavHostFragment(
            fragmentManager,
            fragmentTag,
            navGraphId,
            containerId
    )
    if (navHostFragment.navController.handleDeepLink(intent)
            && selectedItemId != navHostFragment.navController.graph.id) {
        this.selectedItemId = navHostFragment.navController.graph.id
    }
}

private fun detachNavHostFragment(
        fragmentManager: FragmentManager,
        navHostFragment: NavHostFragment
) = fragmentManager.beginTransaction()
        .detach(navHostFragment)
        .commitNow()

private fun attachNavHostFragment(
        fragmentManager: FragmentManager,
        navHostFragment: NavHostFragment,
        isPrimaryNavFragment: Boolean
) = fragmentManager.beginTransaction()
        .attach(navHostFragment)
        .apply {
            if (isPrimaryNavFragment) {
                setPrimaryNavigationFragment(navHostFragment)
            }
        }
        .commitNow()

private fun obtainNavHostFragment(
        fragmentManager: FragmentManager,
        fragmentTag: String,
        navGraphId: Int,
        containerId: Int
): NavHostFragment {
    val existingFragment = fragmentManager.findFragmentByTag(fragmentTag) as NavHostFragment?
    existingFragment?.let { return it }

    return NavHostFragment.create(navGraphId).apply {
        fragmentManager.beginTransaction()
                .add(containerId, this, fragmentTag)
                .commitNow()
    }
}

fun getFragmentTag(index: Int): String {
    if (index < 0) throw IllegalArgumentException()
    return "bottomNavigation#$index"
}

fun clearFragmentManager(transaction: FragmentTransaction, fragmentManager: FragmentManager,
                         navGraphIds: List<Int>, firstFragmentTag: String, excludeTag: String) =
        navGraphIds.forEachIndexed { index, _ ->
            val fragmentTag = getFragmentTag(index)
            if (fragmentTag != firstFragmentTag && fragmentTag != excludeTag) {
                (fragmentManager.findFragmentByTag(fragmentTag) as NavHostFragment?)?.let {
                    transaction.remove(it)
                }
            }
        }
