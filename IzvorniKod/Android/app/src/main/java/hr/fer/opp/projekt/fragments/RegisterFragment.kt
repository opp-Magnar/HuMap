package hr.fer.opp.projekt.fragments


import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ImageView
import androidx.lifecycle.ViewModelProviders
import com.google.android.material.snackbar.Snackbar
import com.theartofdev.edmodo.cropper.CropImage
import com.theartofdev.edmodo.cropper.CropImageView
import hr.fer.opp.projekt.R
import hr.fer.opp.projekt.controllers.FragmentController
import hr.fer.opp.projekt.model.LoginViewModel
import hr.fer.opp.projekt.model.RegisterViewModel
import hr.fer.opp.projekt.model.Role
import hr.fer.opp.projekt.model.User
import hr.fer.opp.projekt.util.listeners.RegistrationListener
import kotlinx.android.synthetic.main.fragment_register.*

class RegisterFragment : FragmentController(), RegistrationListener {

    private val roles by lazy { arrayOf(Role.VODITELJ.toString(), Role.KARTOGRAF.toString(), Role.SPASIOC.toString()) }
    private val registerViewModel: RegisterViewModel by lazy { ViewModelProviders.of(requireActivity()).get(RegisterViewModel::class.java) }
    private var imageUri: Uri? = null
    private lateinit var ivPhoto: ImageView

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? =
            inflater.inflate(R.layout.fragment_register, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupDropdown()
        fabRegisterAddPhoto.setOnClickListener {
            CropImage.activity()
                    .setGuidelines(CropImageView.Guidelines.ON)
                    .start(context!!, this)
        }
        btnCreate.setOnClickListener {
            val up: UserPassword? = validateUserInput() ?: return@setOnClickListener
            registerViewModel.createUser(this, up!!.user, up.password, imageUri)
        }
        ivPhoto = view.findViewById(R.id.ivPhoto)
        ivPhoto.setImageResource(R.drawable.person)
    }

    override fun onResume() {
        super.onResume()
        clearErrors()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            val result = CropImage.getActivityResult(data)
            if (resultCode == Activity.RESULT_OK) {
                imageUri = result!!.uri
                ivPhoto.setImageURI(imageUri)
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                val error = result!!.error
                Snackbar.make(registerFragmentContainer!!, error.message!!, Snackbar.LENGTH_LONG).show()
            }
        }
    }

    private fun clearErrors() {
        tiRole!!.error = null
        tiRegisterUsername!!.error = null
        tiRegisterFirstName!!.error = null
        tiRegisterLastName!!.error = null
        tiRegisterEmail!!.error = null
        tiRegisterPassword!!.error = null
        tiRegisterConfirmPassword!!.error = null
        tiRegisterPhoneNumber!!.error = null
    }

    private fun validateUserInput(): UserPassword? {
        clearErrors()
        val role = registerRolesDropdown!!.text.toString()
        val username = etRegisterUsername!!.text.toString()
        val firstName = etRegisterFirstName!!.text.toString()
        val lastName = etRegisterLastName!!.text.toString()
        val email = etRegisterEmail!!.text.toString()
        val password = etRegisterPassword!!.text.toString()
        val confirmPassword = etRegisterConfirmPassword!!.text.toString()
        val phoneNumber = etRegisterPhoneNumber!!.text.toString()
        if (imageUri == null || role.isEmpty() || username.isEmpty() || firstName.isEmpty() ||
                lastName.isEmpty() || email.isEmpty() || password.isEmpty() || confirmPassword.isEmpty() ||
                phoneNumber.isEmpty() || password != confirmPassword) {
            if (imageUri == null) {
                Snackbar.make(registerFragmentContainer!!, "Image is not selected", Snackbar.LENGTH_LONG).show()
            }
            if (role.isEmpty()) {
                tiRole!!.error = "Role is not selected"
            }
            if (username.isEmpty()) {
                tiRegisterUsername!!.error = "This field is required"
            }
            if (firstName.isEmpty()) {
                tiRegisterFirstName!!.error = "This field is required"
            }
            if (lastName.isEmpty()) {
                tiRegisterLastName!!.error = "This field is required"
            }
            if (email.isEmpty()) {
                tiRegisterEmail!!.error = "This field is required"
            }
            if (password.isEmpty()) {
                tiRegisterPassword!!.error = "This field is required"
            }
            if (confirmPassword.isEmpty()) {
                tiRegisterConfirmPassword!!.error = "This field is required"
            } else if (password != confirmPassword) {
                tiRegisterConfirmPassword!!.error = "Passwords do not match"
            }
            if (phoneNumber.isEmpty()) {
                tiRegisterPhoneNumber!!.error = "This field is required"
            }
            return null
        }
        val user = User(
                username = username,
                firstName = firstName,
                lastName = lastName,
                role = Role.valueOf(role),
                email = email,
                phoneNumber = phoneNumber,
                active = false)
        return UserPassword(user, password)
    }

    override fun onBeginRegistration() = mListener.begin()

    override fun onRegistered(user: User) {
        mListener.stop()
        back()
    }

    override fun onFailureRegistration(message: String) = onFailure(message)

    private fun setupDropdown() {
        val adapter: ArrayAdapter<String> = ArrayAdapter(
                context!!,
                R.layout.dropdown_menu_item,
                roles)
        registerRolesDropdown.setAdapter(adapter)
        registerRolesDropdown.keyListener = null
    }

    override fun onChanged(authState: LoginViewModel.AuthenticationState?) {
        if (authState === LoginViewModel.AuthenticationState.AUTHENTICATED) {
            mListener.navigateBottomAppBarReset(R.id.map_nav)
        } else {
            userChanged()
        }
    }

    data class UserPassword internal constructor(val user: User, val password: String)

}

