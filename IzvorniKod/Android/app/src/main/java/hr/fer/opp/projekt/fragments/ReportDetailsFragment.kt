package hr.fer.opp.projekt.fragments


import android.app.Activity
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.*
import android.view.View.OnTouchListener
import android.view.inputmethod.InputMethodManager
import android.widget.ImageView
import androidx.core.content.ContextCompat
import androidx.core.widget.addTextChangedListener
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.AdapterDataObserver
import com.bumptech.glide.request.RequestOptions
import com.google.firebase.firestore.FirebaseFirestore
import hr.fer.opp.projekt.R
import hr.fer.opp.projekt.controllers.FragmentController
import hr.fer.opp.projekt.model.*
import hr.fer.opp.projekt.util.GlideApp
import hr.fer.opp.projekt.util.adapters.AdapterComment
import hr.fer.opp.projekt.util.listeners.AuthListener
import kotlinx.android.synthetic.main.fragment_report_details.*
import java.text.SimpleDateFormat
import java.util.*

class ReportDetailsFragment : FragmentController(), AuthListener {

    private lateinit var report: Report

    private lateinit var ivPhoto: ImageView

    private lateinit var adapter: AdapterComment

    private val db by lazy { FirebaseFirestore.getInstance() }

    private val colorDrawableBackground by lazy { ColorDrawable(Color.parseColor("#ff0000")) }
    private val deleteIcon by lazy { ContextCompat.getDrawable(context!!, R.drawable.ic_delete)!! }

    private var swipeEnabled = false

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? =
            inflater.inflate(R.layout.fragment_report_details, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        ivPhoto = view.findViewById(R.id.ivPhoto)
        ivPhoto.setImageResource(R.drawable.photo)
        report = arguments!!.getSerializable("report") as Report
        tvReportDescription!!.text = report.description
        report.photoUrl?.let {
            GlideApp.with(context!!)
                    .load(it)
                    .into(ivPhoto)
        }
        tvUsernameReportDetails.text = report.displayName
        tvDateReportDetails.text = SimpleDateFormat("dd.MM.yyyy.", Locale.UK).format(report.creationDate)
        report.userUid?.let { loginViewModel.getUserObjectAsync(it, this) }

        adapter = AdapterComment(report.comments, context!!, loginViewModel)
        tvNumberOfComments.text = report.comments.size.toString()
        adapter.registerAdapterDataObserver(object : AdapterDataObserver() {
            override fun onItemRangeInserted(positionStart: Int, itemCount: Int) {
                onChanged()
            }

            override fun onItemRangeRemoved(positionStart: Int, itemCount: Int) {
                onChanged()
            }

            override fun onChanged() {
                tvNumberOfComments.text = report.comments.size.toString()
            }
        })
        rvComments.setHasFixedSize(true)
        rvComments.layoutManager = LinearLayoutManager(context)
        rvComments.adapter = adapter

        enableCommentDeletion()

        etNewComment.addTextChangedListener {
            etNewComment.text?.let {
                if (it.toString().isNotEmpty()) {
                    etNewComment.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_create, 0)
                    setOnTouchListener()
                } else {
                    etNewComment.setOnTouchListener { _, _ ->
                        false
                    }
                    etNewComment.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0)
                }
            }
        }
        setHasOptionsMenu(true)
    }

    private fun enableCommentDeletion() {
        val itemTouchHelperCallback = object : ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {
            override fun isItemViewSwipeEnabled(): Boolean = swipeEnabled

            override fun onMove(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder, viewHolder2: RecyclerView.ViewHolder): Boolean =
                    false

            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, swipeDirection: Int) =
                    adapter.removeItem(viewHolder.adapterPosition, viewHolder, this@ReportDetailsFragment)

            override fun onChildDraw(
                    c: Canvas,
                    recyclerView: RecyclerView,
                    viewHolder: RecyclerView.ViewHolder,
                    dX: Float,
                    dY: Float,
                    actionState: Int,
                    isCurrentlyActive: Boolean
            ) {
                val itemView = viewHolder.itemView
                val iconMarginVertical = (viewHolder.itemView.height - deleteIcon.intrinsicHeight) / 2

                if (dX > 0) {
                    colorDrawableBackground.setBounds(itemView.left, itemView.top, dX.toInt(), itemView.bottom)
                    deleteIcon.setBounds(itemView.left + iconMarginVertical, itemView.top + iconMarginVertical,
                            itemView.left + iconMarginVertical + deleteIcon.intrinsicWidth, itemView.bottom - iconMarginVertical)
                } else {
                    colorDrawableBackground.setBounds(itemView.right + dX.toInt(), itemView.top, itemView.right, itemView.bottom)
                    deleteIcon.setBounds(itemView.right - iconMarginVertical - deleteIcon.intrinsicWidth, itemView.top + iconMarginVertical,
                            itemView.right - iconMarginVertical, itemView.bottom - iconMarginVertical)
                    deleteIcon.level = 0
                }

                colorDrawableBackground.draw(c)

                c.save()

                if (dX > 0)
                    c.clipRect(itemView.left, itemView.top, dX.toInt(), itemView.bottom)
                else
                    c.clipRect(itemView.right + dX.toInt(), itemView.top, itemView.right, itemView.bottom)

                deleteIcon.draw(c)

                c.restore()

                super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive)
            }
        }
        val itemTouchHelper = ItemTouchHelper(itemTouchHelperCallback)
        itemTouchHelper.attachToRecyclerView(rvComments)
    }

    private fun setOnTouchListener() = etNewComment.setOnTouchListener(OnTouchListener { _, event ->
        if (event.action == MotionEvent.ACTION_DOWN) {
            if (event.rawX >= etNewComment.right + etNewComment.compoundDrawables[2].bounds.width() * 2) {
                report.comments.add(Comment(loginViewModel.currentUser?.uid, etNewComment.text.toString()))
                adapter.notifyDataSetChanged()
                saveReport()
                hideKeyboard()
                etNewComment.setText("")
                etNewComment.clearFocus()
                tiNewComment.clearFocus()
                return@OnTouchListener true
            }
        }
        false
    })

    override fun onOptionsItemSelected(item: MenuItem): Boolean = when (item.itemId) {
        R.id.menuLockReport -> {
            lockReport()
            true
        }
        R.id.menuDeleteReport -> {
            deleteReport()
            true
        }
        else -> false
    }

    fun saveReport() = loginViewModel.saveReport(report)

    private fun lockReport() {
        mListener.begin()
        report.locked = true
        report.lockedDate = Date()
        db.collection("reports").document(report.documentId).set(report).addOnSuccessListener {
            onSuccess()
            back()
        }.addOnFailureListener { exception: Exception -> onFailure(exception.localizedMessage!!) }
    }

    private fun deleteReport() {
        mListener.begin()
        db.collection("reports").document(report.documentId).delete().addOnSuccessListener {
            onSuccess()
            back()
        }.addOnFailureListener { exception: Exception -> onFailure(exception.localizedMessage!!) }
    }

    private fun hideKeyboard() {
        (activity!!.getSystemService(
                Activity.INPUT_METHOD_SERVICE) as InputMethodManager).hideSoftInputFromWindow(
                activity!!.currentFocus!!.windowToken, 0)
    }

    override fun onChanged(authState: LoginViewModel.AuthenticationState?) {
        userChanged()
        swipeEnabled = false
        authenticatedUserAppBar.clear()
        authenticatedUserAppBar.addAll(authenticatedUserAppBarStart)
        loginViewModel.currentUser?.let {
            if (it.role == Role.ADMIN || it.role == Role.VODITELJ || it.role == Role.SPASIOC) {
                authenticatedUserAppBar.add(R.id.menuLockReport)
                if (it.role != Role.SPASIOC) {
                    authenticatedUserAppBar.add(R.id.menuDeleteReport)
                    swipeEnabled = true
                }
            }
        }
        ivAvatarNewComment.setImageResource(R.drawable.person)
        loginViewModel.currentUser?.let {
            GlideApp.with(context!!)
                    .load(it.photoUrl)
                    .apply(RequestOptions.circleCropTransform())
                    .into(ivAvatarNewComment)
        }
    }

    override fun onBeginAuth() = Unit

    override fun onAuthenticated(user: User) {
        GlideApp.with(context!!)
                .load(user.photoUrl)
                .apply(RequestOptions.circleCropTransform())
                .into(ivAvatarReportAuthor)
    }

    override fun onNotActivated(user: User) = onAuthenticated(user)

    override fun onFailureAuth(message: String) = onFailure(message)

}
