package hr.fer.opp.projekt.model

import java.io.Serializable

enum class BlockState : Serializable {
    INACTIVE, ACTIVE, REVIEW, FINISHED
}