package hr.fer.opp.projekt.util.adapters


import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.BaseTransientBottomBar
import com.google.android.material.snackbar.Snackbar
import hr.fer.opp.projekt.R
import hr.fer.opp.projekt.fragments.ReportDetailsFragment
import hr.fer.opp.projekt.model.Comment
import hr.fer.opp.projekt.model.LoginViewModel
import hr.fer.opp.projekt.util.holders.CommentHolder

class AdapterComment(private val comments: ArrayList<Comment>, private val context: Context, private val loginViewModel: LoginViewModel) :
        RecyclerView.Adapter<CommentHolder>() {

    private var removedPosition: Int = 0
    private lateinit var removedItem: Comment

    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int): CommentHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.comment_list_item, parent, false)
        return CommentHolder(view, context, loginViewModel)
    }

    override fun onBindViewHolder(holder: CommentHolder, position: Int) =
            holder.setComment(comments[position])

    fun removeItem(position: Int, viewHolder: RecyclerView.ViewHolder, reportDetailsFragment: ReportDetailsFragment) {
        removedPosition = position
        removedItem = comments[position]

        comments.removeAt(position)
        notifyItemRemoved(position)

        Snackbar.make(viewHolder.itemView, "Comment deleted", Snackbar.LENGTH_LONG).setAction("UNDO") {
            comments.add(removedPosition, removedItem)
            notifyItemInserted(removedPosition)
        }.addCallback(object : BaseTransientBottomBar.BaseCallback<Snackbar>() {
            override fun onDismissed(transientBottomBar: Snackbar?, event: Int) {
                reportDetailsFragment.saveReport()
            }
        }).show()
    }

    override fun getItemCount() = comments.size

}