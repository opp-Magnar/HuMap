package hr.fer.opp.projekt.fragments


import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineDataSet
import hr.fer.opp.projekt.R
import hr.fer.opp.projekt.controllers.StatisticsFragmentController
import hr.fer.opp.projekt.model.Building
import java.util.*
import kotlin.collections.ArrayList

class BuildingStatisticsFragment : StatisticsFragmentController() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? =
            inflater.inflate(R.layout.fragment_building_statistics, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val buildingCreationDataset = LineDataSet(ArrayList<Entry>(), "Buildings created")
        buildingCreationDataset.color = Color.rgb(146, 254, 157)
        buildingCreationDataset.valueFormatter = DayAxisValueFormatter(timeReference)
        buildingCreationDataset.setDrawHighlightIndicators(false)
        buildingCreationDataset.setDrawFilled(true)
        buildingCreationDataset.fillDrawable = ContextCompat.getDrawable(context!!, R.drawable.green_blue_gradient)

        val buildingSearchDataset = LineDataSet(ArrayList<Entry>(), "Buildings searched")
        buildingSearchDataset.color = Color.rgb(237, 33, 58)
        buildingSearchDataset.valueFormatter = DayAxisValueFormatter(timeReference)
        buildingSearchDataset.setDrawHighlightIndicators(false)
        buildingSearchDataset.setDrawFilled(true)
        buildingSearchDataset.fillDrawable = ContextCompat.getDrawable(context!!, R.drawable.fade_red)

        setChartData(buildingCreationDataset, buildingSearchDataset)
        markerSetup("buildings created", "buildings searched")

        db.collection("buildings").orderBy("creationDate").get().addOnSuccessListener {
            val buildings = it.toObjects(Building::class.java)
            val creationDates = buildings.map { building -> building.creationDate }
            val finishDates = buildings.mapNotNull { building -> building.finishDate }.sorted()
            var startDate: Date? = null
            if (creationDates.isNotEmpty() && (finishDates.isEmpty() || creationDates[0] < finishDates[0])) {
                startDate = creationDates[0]
            } else if (creationDates.isNotEmpty()) {
                startDate = finishDates[0]
            }
            fillChart(creationDates, buildingCreationDataset, startDate)
            fillChart(finishDates, buildingSearchDataset, startDate)
        }
    }

}
