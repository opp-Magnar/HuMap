package hr.fer.opp.projekt.util.listeners

import hr.fer.opp.projekt.model.Report
import hr.fer.opp.projekt.util.holders.ReportHolder

interface RecyclerViewOnClickListener {
    fun onClick(holder: ReportHolder, model: Report, position: Int, delete: Boolean)
}