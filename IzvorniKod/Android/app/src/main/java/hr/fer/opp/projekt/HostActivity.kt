package hr.fer.opp.projekt


import android.os.Bundle
import android.os.Handler
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.core.view.iterator
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.NavController
import com.google.android.material.snackbar.Snackbar
import hr.fer.opp.projekt.fragments.EditProfileFragment
import hr.fer.opp.projekt.model.LoginViewModel
import hr.fer.opp.projekt.model.LoginViewModel.AuthenticationState
import hr.fer.opp.projekt.util.listeners.OnFragmentInteractionListener
import hr.fer.opp.projekt.util.reset
import hr.fer.opp.projekt.util.setupWithNavController
import kotlinx.android.synthetic.main.activity_host.*

class HostActivity : AppCompatActivity(), OnFragmentInteractionListener, Observer<AuthenticationState> {

    // TODO: enable user profile data change

    private val loginViewModel: LoginViewModel by lazy { ViewModelProviders.of(this).get(LoginViewModel::class.java) }
    private var appBarMenuItems: List<Int> = emptyList()
    private lateinit var currentNavController: LiveData<NavController>

    private val navGraphIds = listOf(R.navigation.reports_nav,
            R.navigation.auth_nav, R.navigation.map_nav, R.navigation.stats_nav, R.navigation.users_nav)

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.AppTheme)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_host)
        loginViewModel.authenticationState.observe(this, this)
        savedInstanceState ?: setupBottomNavBar()
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
        setupBottomNavBar()
    }

    private fun setupBottomNavBar() {
        currentNavController = bottomNavBar.setupWithNavController(navGraphIds, supportFragmentManager, R.id.nav_host_container, intent)
    }

    override fun showAppBarItems(visibleMenuItems: List<Int>) {
        appBarMenuItems = visibleMenuItems
        invalidateOptionsMenu()
    }

    override fun navigateBottomAppBar(id: Int) {
        bottomNavBar.selectedItemId = id
    }

    override fun navigateBottomAppBarReset(id: Int) {
        reset = true
        navigateBottomAppBar(id)
    }

    override fun begin() {
        progressView.visibility = View.VISIBLE
    }

    override fun stop() {
        progressView.visibility = View.GONE
    }

    override fun showSnackbar(text: String) =
            Snackbar.make(mainCoordinatorLayout!!, text, Snackbar.LENGTH_LONG).show()

    override fun showBottomNavBarItems(visibleMenuItems: List<Int>) =
            showMenuItems(bottomNavBar.menu, visibleMenuItems)

    override fun showBottomNavBar() {
        bottomNavBar.isVisible = true
    }

    override fun hideBottomNavBar() {
        bottomNavBar.isVisible = false
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.app_bar_menu, menu)
        showMenuItems(menu, appBarMenuItems)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean =
            when (item.itemId) {
                R.id.menuSignOut -> {
                    loginViewModel.signOut()
                    true
                }
                R.id.menuEditProfile -> {
                    Handler().post {
                        currentNavController.value!!.navigate(R.id.editProfile)
                    }
                    true
                }
                else -> false
            }

    override fun onSupportNavigateUp(): Boolean = currentNavController.value?.navigateUp() ?: false

    private fun showMenuItems(menu: Menu, visibleMenuItems: List<Int>) {
        for (item in menu) {
            item.isVisible = visibleMenuItems.contains(item.itemId)
        }
    }

    override fun onChanged(authState: AuthenticationState?) {
        if (authState === AuthenticationState.IN_PROGRESS) {
            launchView.visibility = View.VISIBLE
        } else {
            launchView.visibility = View.GONE
        }
    }

}