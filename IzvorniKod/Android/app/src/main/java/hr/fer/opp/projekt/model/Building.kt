package hr.fer.opp.projekt.model

import com.google.firebase.firestore.DocumentId
import java.io.Serializable
import java.util.*

data class Building(
        @DocumentId val documentId: String = "",
        val polygon: List<Point> = arrayListOf(),
        var state: BuildingState = BuildingState.UNSEARCHED,
        val creationDate: Date = Date(),
        var finishDate: Date? = null
) : Serializable {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Building

        if (documentId != other.documentId) return false

        return true
    }

    override fun hashCode(): Int {
        return documentId.hashCode()
    }
}