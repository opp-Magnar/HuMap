package hr.fer.opp.projekt.util.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.fragment.app.FragmentManager
import com.firebase.ui.firestore.FirestoreRecyclerAdapter
import com.firebase.ui.firestore.FirestoreRecyclerOptions
import hr.fer.opp.projekt.R
import hr.fer.opp.projekt.model.LoginViewModel
import hr.fer.opp.projekt.model.User
import hr.fer.opp.projekt.util.holders.UserHolder

class FirestoreAdapterUser(options: FirestoreRecyclerOptions<User>, private val context: Context,
                           private val loginViewModel: LoginViewModel, private val fragmentManager: FragmentManager) : FirestoreRecyclerAdapter<User, UserHolder>(options) {

    override fun onBindViewHolder(holder: UserHolder, position: Int, model: User) =
            holder.setUser(model, context, loginViewModel, fragmentManager)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.list_item, parent, false)
        return UserHolder(view)
    }

}