package hr.fer.opp.projekt.model


import android.net.Uri
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.EventListener
import com.google.firebase.firestore.FirebaseFirestoreException
import com.google.firebase.firestore.ListenerRegistration
import hr.fer.opp.projekt.util.FirebaseSource
import hr.fer.opp.projekt.util.listeners.AuthListener
import hr.fer.opp.projekt.util.listeners.DatabaseListener

class LoginViewModel : ViewModel(), AuthListener {

    enum class AuthenticationState {
        IN_PROGRESS, UNAUTHENTICATED, AUTHENTICATED, NOT_ACTIVATED
    }

    val authenticationState = MutableLiveData<AuthenticationState>()
    var currentUser: User? = null
        private set

    init {
        authenticationState.value = AuthenticationState.IN_PROGRESS
        FirebaseSource.getCurrentUserObjectAsync(this)
    }

    private val documentListener = EventListener { snapshot: DocumentSnapshot?, e: FirebaseFirestoreException? ->
        if (e != null) {
            signOut()
        } else if (snapshot != null && snapshot.exists()) {
            val user = snapshot.toObject(User::class.java)!!
            if (user.active) {
                setUser(user)
            } else {
                onNotActivated(user)
            }
        } else {
            signOut()
        }
    }

    private var documentListenerRegistration: ListenerRegistration? = null

    private fun addUserDocumentListener(uid: String) {
        documentListenerRegistration = FirebaseSource.addUserDocumentListener(documentListener, uid)
    }

    fun saveReport(report: Report) = FirebaseSource.saveReportChanges(report)

    private fun getCombo(l1: AuthListener, l2: AuthListener): AuthListener =
            object : AuthListener {
                override fun onBeginAuth() {
                    l1.onBeginAuth()
                    l2.onBeginAuth()
                }

                override fun onAuthenticated(user: User) {
                    l1.onAuthenticated(user)
                    l2.onAuthenticated(user)
                }

                override fun onNotActivated(user: User) {
                    l1.onNotActivated(user)
                    l2.onNotActivated(user)
                }

                override fun onFailureAuth(message: String) {
                    l1.onFailureAuth(message)
                    l2.onFailureAuth(message)
                }
            }

    fun authenticate(listener: AuthListener, email: String, password: String) =
            FirebaseSource.authenticate(getCombo(this, listener), email, password)

    fun createReport(listener: DatabaseListener, report: Report, imageUri: Uri?) =
            FirebaseSource.createReport(listener, report, imageUri)

    fun signOut() {
        cleanUser()
        authenticationState.value = AuthenticationState.UNAUTHENTICATED
    }

    fun getUserObjectAsync(userUid: String, listener: AuthListener) =
            FirebaseSource.getUserObjectAsync(userUid, listener)

    override fun onBeginAuth() = Unit

    override fun onAuthenticated(user: User) =
            setUser(user).apply { addUserDocumentListener(user.uid) }

    override fun onNotActivated(user: User) {
        cleanUser()
        authenticationState.value = AuthenticationState.NOT_ACTIVATED
    }

    override fun onFailureAuth(message: String) = signOut()

    fun activateUser(databaseListener: DatabaseListener? = null, user: User) =
            FirebaseSource.activateUser(databaseListener, user)

    fun changeUserRole(listener: DatabaseListener? = null, user: User, role: Role) =
            FirebaseSource.changeUserRole(listener, user, role)

    private fun cleanUser() {
        documentListenerRegistration?.remove()
        FirebaseSource.signOut()
        currentUser = null
    }

    private fun setUser(user: User) {
        currentUser = user
        authenticationState.value = AuthenticationState.AUTHENTICATED
    }

}