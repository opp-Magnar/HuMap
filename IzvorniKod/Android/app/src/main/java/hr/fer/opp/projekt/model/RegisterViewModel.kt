package hr.fer.opp.projekt.model

import android.net.Uri
import androidx.lifecycle.ViewModel
import hr.fer.opp.projekt.util.FirebaseSource
import hr.fer.opp.projekt.util.listeners.RegistrationListener

class RegisterViewModel : ViewModel(), RegistrationListener {

    private fun getCombo(l1: RegistrationListener, l2: RegistrationListener): RegistrationListener =
            object : RegistrationListener {
                override fun onBeginRegistration() {
                    l1.onBeginRegistration()
                    l2.onBeginRegistration()
                }

                override fun onRegistered(user: User) {
                    l1.onRegistered(user)
                    l2.onRegistered(user)
                }

                override fun onFailureRegistration(message: String) {
                    l1.onFailureRegistration(message)
                    l2.onFailureRegistration(message)
                }
            }

    fun createUser(listener: RegistrationListener, user: User?, password: String?, imageUri: Uri?) =
            FirebaseSource.createUser(getCombo(this, listener), user!!, password!!, imageUri!!)

    override fun onBeginRegistration() = Unit

    override fun onRegistered(user: User) = FirebaseSource.signOut()

    override fun onFailureRegistration(message: String) = FirebaseSource.signOut()

}