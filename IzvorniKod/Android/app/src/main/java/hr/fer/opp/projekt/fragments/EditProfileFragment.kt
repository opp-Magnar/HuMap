package hr.fer.opp.projekt.fragments


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ImageView
import com.google.firebase.firestore.FirebaseFirestore
import hr.fer.opp.projekt.R
import hr.fer.opp.projekt.controllers.FragmentController
import hr.fer.opp.projekt.model.Role
import hr.fer.opp.projekt.model.User
import hr.fer.opp.projekt.util.GlideApp
import kotlinx.android.synthetic.main.fragment_register.*

class EditProfileFragment : FragmentController() {

    private val db = FirebaseFirestore.getInstance()

    private val roles by lazy { arrayOf(Role.VODITELJ.toString(), Role.KARTOGRAF.toString(), Role.SPASIOC.toString()) }
    private lateinit var ivPhoto: ImageView

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? =
            inflater.inflate(R.layout.fragment_edit_profile, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupDropdown()
        ivPhoto = view.findViewById(R.id.ivPhoto)
        fillData()
    }

    override fun onStart() {
        super.onStart()
        mListener.hideBottomNavBar()
        clearErrors()
    }

    override fun onStop() {
        super.onStop()
        mListener.showBottomNavBar()
    }

    private fun fillData() {
        val user = loginViewModel.currentUser!!
        registerRolesDropdown!!.setText(user.role.toString(), false)
        etRegisterUsername!!.setText(user.username)
        etRegisterFirstName!!.setText(user.firstName)
        etRegisterLastName!!.setText(user.lastName)
        etRegisterPhoneNumber!!.setText(user.phoneNumber)

        btnCreate.setOnClickListener {
            if (!validateUserInput(user)) return@setOnClickListener
            mListener.begin()
            db.collection("users").document(user.uid).set(user).addOnSuccessListener {
                mListener.stop()
                back()
            }.addOnFailureListener {exception ->
                mListener.stop()
                mListener.showSnackbar(exception.localizedMessage!!)
            }
        }

        ivPhoto.setImageResource(R.drawable.person)
        GlideApp.with(context!!)
                .load(user.photoUrl)
                .into(ivPhoto)
    }

    private fun setupDropdown() {
        val adapter: ArrayAdapter<String> = ArrayAdapter(
                context!!,
                R.layout.dropdown_menu_item,
                roles)
        registerRolesDropdown.setAdapter(adapter)
        registerRolesDropdown.keyListener = null
    }

    private fun validateUserInput(user: User): Boolean {
        clearErrors()
        val role = registerRolesDropdown!!.text.toString()
        val username = etRegisterUsername!!.text.toString()
        val firstName = etRegisterFirstName!!.text.toString()
        val lastName = etRegisterLastName!!.text.toString()
        val phoneNumber = etRegisterPhoneNumber!!.text.toString()
        return if (role.isEmpty() || username.isEmpty() || firstName.isEmpty() || lastName.isEmpty() || phoneNumber.isEmpty()) {
            if (role.isEmpty()) {
                tiRole!!.error = "Role is not selected"
            }
            if (username.isEmpty()) {
                tiRegisterUsername!!.error = "This field is required"
            }
            if (firstName.isEmpty()) {
                tiRegisterFirstName!!.error = "This field is required"
            }
            if (lastName.isEmpty()) {
                tiRegisterLastName!!.error = "This field is required"
            }
            if (phoneNumber.isEmpty()) {
                tiRegisterPhoneNumber!!.error = "This field is required"
            }
            false
        } else {
            user.username = username
            user.firstName = firstName
            user.lastName = lastName
            user.role = Role.valueOf(role)
            user.phoneNumber = phoneNumber
            true
        }
    }

    private fun clearErrors() {
        tiRole!!.error = null
        tiRegisterUsername!!.error = null
        tiRegisterFirstName!!.error = null
        tiRegisterLastName!!.error = null
        tiRegisterPhoneNumber!!.error = null
    }

}
