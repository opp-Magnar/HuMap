package hr.fer.opp.projekt.util.listeners

interface DatabaseListener {
    fun onBegin()
    fun onSuccess()
    fun onFailure(message: String)
}