package hr.fer.opp.projekt.fragments


import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineDataSet
import hr.fer.opp.projekt.R
import hr.fer.opp.projekt.controllers.StatisticsFragmentController
import hr.fer.opp.projekt.model.Report
import java.util.*
import kotlin.collections.ArrayList

class ReportStatisticsFragment : StatisticsFragmentController() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? =
            inflater.inflate(R.layout.fragment_report_statistics, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val reportCreationDataset = LineDataSet(ArrayList<Entry>(), "Reports created")
        reportCreationDataset.color = Color.rgb(146, 254, 157)
        reportCreationDataset.valueFormatter = DayAxisValueFormatter(timeReference)
        reportCreationDataset.setDrawHighlightIndicators(false)
        reportCreationDataset.setDrawFilled(true)
        reportCreationDataset.fillDrawable = ContextCompat.getDrawable(context!!, R.drawable.green_blue_gradient)

        val reportLockedDataset = LineDataSet(ArrayList<Entry>(), "People found")
        reportLockedDataset.color = Color.rgb(237, 33, 58)
        reportLockedDataset.valueFormatter = DayAxisValueFormatter(timeReference)
        reportLockedDataset.setDrawHighlightIndicators(false)
        reportLockedDataset.setDrawFilled(true)
        reportLockedDataset.fillDrawable = ContextCompat.getDrawable(context!!, R.drawable.fade_red)

        setChartData(reportCreationDataset, reportLockedDataset)
        markerSetup("reports created", "people found")

        db.collection("reports").orderBy("creationDate").get().addOnSuccessListener {
            val reports = it.toObjects(Report::class.java)
            val creationDates = reports.map { report -> report.creationDate }
            val lockedDates = reports.mapNotNull { report -> report.lockedDate }.sorted()
            var startDate: Date? = null
            if (creationDates.isNotEmpty() && (lockedDates.isEmpty() || creationDates[0] < lockedDates[0])) {
                startDate = creationDates[0]
            } else if (creationDates.isNotEmpty()) {
                startDate = lockedDates[0]
            }
            fillChart(creationDates, reportCreationDataset, startDate)
            fillChart(lockedDates, reportLockedDataset, startDate)
        }
    }

}
