package hr.fer.opp.projekt


import hr.fer.opp.projekt.controllers.getFormattedDate
import hr.fer.opp.projekt.fragments.*
import hr.fer.opp.projekt.model.Role
import hr.fer.opp.projekt.model.User
import hr.fer.opp.projekt.util.getFragmentTag
import org.junit.Assert
import org.junit.Rule
import org.junit.Test
import org.junit.rules.ExpectedException
import java.text.SimpleDateFormat
import java.util.*


class JUnitTests {

    @Test
    fun testMapPermissionsNull() {
        Assert.assertFalse(canAddRegions(null))
        Assert.assertFalse(canAddBlocks(null))
        Assert.assertFalse(canAddBuildings(null))
        Assert.assertFalse(canChangeBlockState(null))
        Assert.assertFalse(canChangeBuildingState(null))
        Assert.assertFalse(canSeeRegionsAndBlocks(null))
    }

    @Test
    fun testMapPermissionsAdmin() {
        val user = User(role = Role.ADMIN)
        Assert.assertTrue(canAddRegions(user))
        Assert.assertTrue(canAddBlocks(user))
        Assert.assertTrue(canAddBuildings(user))
        Assert.assertTrue(canChangeBlockState(user))
        Assert.assertTrue(canChangeBuildingState(user))
        Assert.assertTrue(canSeeRegionsAndBlocks(user))
    }

    @Test
    fun testMapPermissionsKartograf() {
        val user = User(role = Role.KARTOGRAF)
        Assert.assertFalse(canAddRegions(user))
        Assert.assertFalse(canAddBlocks(user))
        Assert.assertTrue(canAddBuildings(user))
        Assert.assertTrue(canChangeBlockState(user))
        Assert.assertFalse(canChangeBuildingState(user))
        Assert.assertTrue(canSeeRegionsAndBlocks(user))
    }

    @Test
    fun testMapPermissionsSpasioc() {
        val user = User(role = Role.SPASIOC)
        Assert.assertFalse(canAddRegions(user))
        Assert.assertFalse(canAddBlocks(user))
        Assert.assertFalse(canAddBuildings(user))
        Assert.assertFalse(canChangeBlockState(user))
        Assert.assertTrue(canChangeBuildingState(user))
        Assert.assertFalse(canSeeRegionsAndBlocks(user))
    }

    @Test
    fun testMapPermissionsVoditelj() {
        val user = User(role = Role.VODITELJ)
        Assert.assertTrue(canAddRegions(user))
        Assert.assertTrue(canAddBlocks(user))
        Assert.assertTrue(canAddBuildings(user))
        Assert.assertFalse(canChangeBlockState(user))
        Assert.assertFalse(canChangeBuildingState(user))
        Assert.assertTrue(canSeeRegionsAndBlocks(user))
    }

    // 1.1.2016.
    private val timeReference = 1451602800000L

    @Test
    fun testDateFormattingShort() {
        val mDataFormat = SimpleDateFormat("dd/MM/yy", Locale.UK)
        Assert.assertEquals("01/01/16", getFormattedDate(timeReference, mDataFormat))
    }

    @Test
    fun testDateFormattingLong() {
        val mDataFormat = SimpleDateFormat("dd.MM.yyyy.", Locale.UK)
        Assert.assertEquals("01.01.2016.", getFormattedDate(timeReference, mDataFormat))
    }

    @Test
    fun testFragmentTagNormal() {
        Assert.assertEquals("bottomNavigation#1", getFragmentTag(1))
        Assert.assertEquals("bottomNavigation#2", getFragmentTag(2))
    }

    @get:Rule
    var exception: ExpectedException = ExpectedException.none()

    @Test
    fun testFragmentTagUnexpected() {
        exception.expect(IllegalArgumentException::class.java)
        getFragmentTag(-1)
        getFragmentTag(-2)
    }

}