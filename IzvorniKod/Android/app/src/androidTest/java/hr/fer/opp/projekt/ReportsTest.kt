package hr.fer.opp.projekt


import android.view.View
import android.view.ViewGroup
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.filters.LargeTest
import androidx.test.rule.ActivityTestRule
import androidx.test.runner.AndroidJUnit4
import org.hamcrest.Description
import org.hamcrest.Matcher
import org.hamcrest.Matchers.`is`
import org.hamcrest.Matchers.allOf
import org.hamcrest.TypeSafeMatcher
import org.hamcrest.core.IsInstanceOf
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@LargeTest
@RunWith(AndroidJUnit4::class)
class ReportsTest {

    @Rule
    @JvmField
    var mActivityTestRule = ActivityTestRule(HostActivity::class.java)

    @Test
    fun reportsTest() {
        // Added a sleep statement to match the app's execution delay.
        // The recommended way to handle such scenarios is to use Espresso idling resources:
        // https://google.github.io/android-testing-support-library/docs/espresso/idling-resource/index.html
        Thread.sleep(7000)

        val materialCardView = onView(
                allOf(withId(R.id.cardHolderReport),
                        childAtPosition(
                                allOf(withId(R.id.rvReports),
                                        childAtPosition(
                                                withClassName(`is`("androidx.coordinatorlayout.widget.CoordinatorLayout")),
                                                1)),
                                0),
                        isDisplayed()))
        materialCardView.perform(click())

        val textView = onView(
                allOf(withText("Details"),
                        childAtPosition(
                                allOf(withId(R.id.fragmentToolbar),
                                        childAtPosition(
                                                IsInstanceOf.instanceOf(android.widget.FrameLayout::class.java),
                                                1)),
                                1),
                        isDisplayed()))
        textView.check(matches(withText("Details")))

        val appCompatImageButton = onView(
                allOf(withContentDescription("Navigate up"),
                        childAtPosition(
                                allOf(withId(R.id.fragmentToolbar),
                                        childAtPosition(
                                                withClassName(`is`("com.google.android.material.appbar.CollapsingToolbarLayout")),
                                                1)),
                                1),
                        isDisplayed()))
        appCompatImageButton.perform(click())

        val floatingActionButton = onView(
                allOf(withId(R.id.fabAddReport),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.nav_host_container),
                                        0),
                                2),
                        isDisplayed()))
        floatingActionButton.perform(click())

        val textView2 = onView(
                allOf(withText("New"),
                        childAtPosition(
                                allOf(withId(R.id.fragmentToolbar),
                                        childAtPosition(
                                                IsInstanceOf.instanceOf(android.widget.FrameLayout::class.java),
                                                1)),
                                1),
                        isDisplayed()))
        textView2.check(matches(withText("New")))
    }

    private fun childAtPosition(
            parentMatcher: Matcher<View>, position: Int): Matcher<View> {

        return object : TypeSafeMatcher<View>() {
            override fun describeTo(description: Description) {
                description.appendText("Child at position $position in parent ")
                parentMatcher.describeTo(description)
            }

            public override fun matchesSafely(view: View): Boolean {
                val parent = view.parent
                return parent is ViewGroup && parentMatcher.matches(parent)
                        && view == parent.getChildAt(position)
            }
        }
    }
}
