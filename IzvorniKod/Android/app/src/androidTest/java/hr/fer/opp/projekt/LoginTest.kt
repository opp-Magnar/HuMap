package hr.fer.opp.projekt


import android.view.View
import android.view.ViewGroup
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.Espresso.pressBack
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.filters.LargeTest
import androidx.test.rule.ActivityTestRule
import androidx.test.runner.AndroidJUnit4
import org.hamcrest.Description
import org.hamcrest.Matcher
import org.hamcrest.Matchers.`is`
import org.hamcrest.Matchers.allOf
import org.hamcrest.TypeSafeMatcher
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@LargeTest
@RunWith(AndroidJUnit4::class)
class LoginTest {

    @Rule
    @JvmField
    var mActivityTestRule = ActivityTestRule(HostActivity::class.java)

    @Test
    fun loginTest() {
        // Added a sleep statement to match the app's execution delay.
        // The recommended way to handle such scenarios is to use Espresso idling resources:
        // https://google.github.io/android-testing-support-library/docs/espresso/idling-resource/index.html
        Thread.sleep(7000)

        val bottomNavigationItemView = onView(
                allOf(withId(R.id.auth_nav), withContentDescription("Login"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.bottomNavBar),
                                        0),
                                1),
                        isDisplayed()))
        bottomNavigationItemView.perform(click())

        val textInputEditText = onView(
                allOf(withId(R.id.etLoginEmail),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.tiLoginEmail),
                                        0),
                                0),
                        isDisplayed()))
        textInputEditText.perform(replaceText("filip.husnjak98@gmail.com"), closeSoftKeyboard())

        val textInputEditText2 = onView(
                allOf(withId(R.id.etLoginPassword),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.tiLoginPassword),
                                        0),
                                0),
                        isDisplayed()))
        textInputEditText2.perform(replaceText("Qwer123"), closeSoftKeyboard())

        val materialButton = onView(
                allOf(withId(R.id.btnSignIn), withText("Sign In"),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(`is`("androidx.core.widget.NestedScrollView")),
                                        0),
                                2),
                        isDisplayed()))
        materialButton.perform(click())

        // Added a sleep statement to match the app's execution delay.
        // The recommended way to handle such scenarios is to use Espresso idling resources:
        // https://google.github.io/android-testing-support-library/docs/espresso/idling-resource/index.html
        Thread.sleep(7000)

        val textView = onView(
                allOf(withText("Map"),
                        childAtPosition(
                                allOf(withId(R.id.fragmentToolbar),
                                        childAtPosition(
                                                withId(R.id.include),
                                                0)),
                                0),
                        isDisplayed()))
        textView.check(matches(withText("Map")))
    }

    private fun childAtPosition(
            parentMatcher: Matcher<View>, position: Int): Matcher<View> {

        return object : TypeSafeMatcher<View>() {
            override fun describeTo(description: Description) {
                description.appendText("Child at position $position in parent ")
                parentMatcher.describeTo(description)
            }

            public override fun matchesSafely(view: View): Boolean {
                val parent = view.parent
                return parent is ViewGroup && parentMatcher.matches(parent)
                        && view == parent.getChildAt(position)
            }
        }
    }
}
