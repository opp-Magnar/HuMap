package hr.fer.opp.projekt


import android.view.View
import android.view.ViewGroup
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.filters.LargeTest
import androidx.test.rule.ActivityTestRule
import androidx.test.runner.AndroidJUnit4
import org.hamcrest.Description
import org.hamcrest.Matcher
import org.hamcrest.Matchers.`is`
import org.hamcrest.Matchers.allOf
import org.hamcrest.TypeSafeMatcher
import org.hamcrest.core.IsInstanceOf
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@LargeTest
@RunWith(AndroidJUnit4::class)
class RegistrationTest {

    @Rule
    @JvmField
    var mActivityTestRule = ActivityTestRule(HostActivity::class.java)

    @Test
    fun registrationTest() {
        // Added a sleep statement to match the app's execution delay.
        // The recommended way to handle such scenarios is to use Espresso idling resources:
        // https://google.github.io/android-testing-support-library/docs/espresso/idling-resource/index.html
        Thread.sleep(7000)

        val bottomNavigationItemView = onView(
                allOf(withId(R.id.auth_nav), withContentDescription("Login"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.bottomNavBar),
                                        0),
                                1),
                        isDisplayed()))
        bottomNavigationItemView.perform(click())

        val materialButton = onView(
                allOf(withId(R.id.btnRegister), withText("Create account"),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(`is`("androidx.core.widget.NestedScrollView")),
                                        0),
                                3),
                        isDisplayed()))
        materialButton.perform(click())

        val textView = onView(
                allOf(withText("Sign Up"),
                        childAtPosition(
                                allOf(withId(R.id.fragmentToolbar),
                                        childAtPosition(
                                                IsInstanceOf.instanceOf(android.widget.FrameLayout::class.java),
                                                1)),
                                1),
                        isDisplayed()))
        textView.check(matches(withText("Sign Up")))

        val bottomNavigationItemView2 = onView(
                allOf(withId(R.id.reports_nav), withContentDescription("Reports"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.bottomNavBar),
                                        0),
                                0),
                        isDisplayed()))
        bottomNavigationItemView2.perform(click())
    }

    private fun childAtPosition(
            parentMatcher: Matcher<View>, position: Int): Matcher<View> {

        return object : TypeSafeMatcher<View>() {
            override fun describeTo(description: Description) {
                description.appendText("Child at position $position in parent ")
                parentMatcher.describeTo(description)
            }

            public override fun matchesSafely(view: View): Boolean {
                val parent = view.parent
                return parent is ViewGroup && parentMatcher.matches(parent)
                        && view == parent.getChildAt(position)
            }
        }
    }
}
